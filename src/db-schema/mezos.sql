-- Open Source License
-- Copyright (c) 2018-2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DROP FUNCTION contracts2(character varying);
CREATE OR REPLACE FUNCTION contracts2 (x varchar)
RETURNS TABLE(k char, bal bigint, operation_hash char)
AS $$
SELECT x, coalesce((SELECT balance FROM contract_balance WHERE address = x order by block_level desc limit 1), 0), null
UNION ALL
SELECT k, coalesce((SELECT balance FROM contract_balance WHERE address = k order by block_level desc limit 1), 0), operation_hash FROM origination o WHERE o.source = x
$$ LANGUAGE SQL;

-- CREATE OR REPLACE FUNCTION contracts3 (x varchar)
-- RETURNS TABLE(k char, bal numeric, operation_hash char, delegate char)
-- AS $$
-- SELECT x, coalesce((SELECT sum(diff) FROM balance where contract_address = x), 0), null, null
-- UNION ALL
-- SELECT k, coalesce((SELECT sum(diff) FROM balance WHERE contract_address = k), 0), operation_hash, c.delegate FROM origination o, contract c WHERE o.source = x and o.k is not null and c.address = o.k
-- $$ LANGUAGE SQL;


select 'creating function latest_balance' as action;
CREATE OR REPLACE FUNCTION latest_balance (x varchar)
RETURNS TABLE(bal bigint)
AS $$
select coalesce((
SELECT c.balance
FROM contract_balance c, block b
WHERE c.address = x
  and c.block_hash = b.hash
--OPT  and not b.rejected
order by c.block_level desc limit 1
), 0) as bal
$$ LANGUAGE SQL;


-- DROP FUNCTION contracts3;
CREATE OR REPLACE FUNCTION contracts3 (x varchar)
RETURNS TABLE(k char, bal bigint, operation_hash char, delegate char, storage jsonb)
AS $$
SELECT x, (select latest_balance (x)), null, null, null
UNION ALL
SELECT
  k,
  (select latest_balance (k)),
  operation_hash,
  (select delegate from contract c2 where c2.delegate is not null and c2.address = k order by c2.autoid desc limit 1),
  (select c.script->'storage' as storage from contract c where c.script is not null and c.address = k order by c.autoid desc limit 1)
FROM origination o
WHERE o.source = x and o.k is not null
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION manager (x varchar)
RETURNS TABLE(pkh char)
AS $$
select coalesce(
(select tx.source
from tx
where tx.destination = x
limit 1),
(select o.source
from origination o
where o.k = x)
)
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_reveal (address varchar)
RETURNS TABLE(
type text,
id integer,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'reveal', -- type
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       oa.hash, -- hash
       address, -- source
       bal.diff, -- fees (bal.balance_kind = 2)
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       i.pk, -- public_key (reveal)
       cast(null as bigint), -- amount (tx)
       null, -- destination (tx)
       null, -- parameters (tx)
       null, -- entrypoint (tx)
       null, -- contract_address (origination)
       null  -- delegate (delegation)
from
   operation_alpha oa,
   block b,
   manager_numbers m,
   implicit i,
   operation op,
   tx t,
   balance bal
where
i.pkh = address
AND i.revealed = op.hash
AND oa.operation_kind = 7
AND op.hash = oa.hash
AND b.hash = op.block_hash
AND m.hash = oa.hash
and m.id = oa.id
and t.source = address
and t.operation_hash = op.hash
and t.op_id = oa.id
and bal.operation_hash = op.hash
and bal.op_id = oa.id
and bal.balance_kind = 2
--OPT and not b.rejected
-- and bal.contract_address = address
$$ LANGUAGE SQL;
-- select * from get_reveal('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;
select * from get_reveal('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;

CREATE OR REPLACE FUNCTION get_transaction (address varchar)
RETURNS TABLE(
type text,
id integer,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'transaction', oa.autoid, b.level, b.timestamp, b.hash, oa.hash, t.source, t.fee,
       m.counter, m.gas_limit, m.storage_limit, t.op_id,
       null,  -- address (reveal)
       t.amount, -- amount (tx)
       t.destination, -- destination (tx)
       t."parameters", -- parameters (tx)
       t.entrypoint, -- entrypoint (tx)
       null, -- contract_address (origination)
       null -- delegate (delegation)
from operation_alpha oa, tx t, manager_numbers m
, operation op
, block b
where op.hash = oa.hash AND
 (address = t.destination or address = t.source)
and oa.operation_kind = 8 AND oa.hash = t.operation_hash AND m.hash = oa.hash
and m.id = oa.id
and b.hash = op.block_hash
and oa.id = t.op_id
--OPT and not b.rejected
$$ LANGUAGE SQL;
select * from get_transaction('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;
select * from get_transaction('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;

CREATE OR REPLACE FUNCTION get_origination (address varchar)
RETURNS TABLE(
type text,
id integer,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'origination', oa.autoid, b.level, b.timestamp, b.hash, oa.hash,
       o.source,
       -- cast(null as bigint), -- bal.diff,
       (select bal.diff from balance bal where bal.operation_hash = op.hash and bal.balance_kind = 2),
       m.counter, m.gas_limit, m.storage_limit, o.op_id,
       null,
       cast(null as bigint), -- amount (tx)
       null, -- destination (tx)
       null, -- parameters (tx)
       null, -- entrypoint (tx)
       o.k, -- contract_address (origination)
       null -- delegate (delegation)
from operation_alpha oa, manager_numbers m, origination o, block b, operation op
--     , balance bal
where (address = o.source or address = o.k) and o.operation_hash = oa.hash
and oa.operation_kind = 9
AND oa.hash = op.hash
AND m.hash = oa.hash
and m.id = oa.id
and oa.id = o.op_id
and op.block_hash = b.hash
--OPT and not b.rejected
-- and bal.operation_hash = op.hash and bal.balance_kind = 2
$$ LANGUAGE SQL;
select * from get_origination('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;


CREATE OR REPLACE FUNCTION get_delegation (address varchar)
RETURNS TABLE(
type text,
id integer,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'delegation', oa.autoid, b.level, b.timestamp, b.hash, oa.hash,
       d.source,
       bal.diff, -- cast(null as bigint),
       m.counter, m.gas_limit, m.storage_limit, d.op_id,
       null, -- public_key (revelation)
       cast(null as bigint), --amount (tx)
       null, -- destination (tx)
       null, -- parameters (tx)
       null, -- entrypoint (tx)
       null, -- contract_address (origination)
       pkh -- delegate (delegation)
from operation_alpha oa, manager_numbers m, delegation d, block b, operation op, balance bal
where
 (address = d.pkh or address = d.source) and d.operation_hash = oa.hash
AND m.hash = oa.hash
and m.id = oa.id
and oa.hash = op.hash
and op.block_hash = b.hash
and oa.operation_kind = 10
and bal.operation_hash = op.hash
and bal.op_id = oa.id
and bal.balance_kind = 2
--OPT and not b.rejected
$$ LANGUAGE SQL;
select * from get_delegation('tz1hGaDz45yCG1AbZqwS653KFDcvmv6jUVqW') limit 10;;
select * from get_delegation('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;

create or replace function get_operations (address varchar, lastid integer, lim integer)
RETURNS TABLE(
type text,
id integer,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select * from get_delegation(address) where id < lastid order by id desc limit lim)
union
(select * from get_origination(address) where id < lastid order by id desc limit lim)
union
(select * from get_transaction(address) where id < lastid order by id desc limit lim)
union
(select * from get_reveal(address)  where id < lastid order by id desc limit lim)
)
order by id desc limit lim
$$ language sql;


-- drop function get_operations ;
-- drop function get_transaction ;
-- drop function get_reveal ;
-- drop function get_origination ;
-- drop function get_delegation ;
