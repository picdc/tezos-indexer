-----------------------------------------------------------------------------
-- DO NOT BLINDLY RUN THE CONTENTS OF THIS FILE
-----------------------------------------------------------------------------

-- If ever this file is blindly run, it should only consume time without
-- having any actual effect on the database: everything should be inside SQL
-- transactions that are rolled back no matter what.

-----------------------------------------------------------------------------
-- BETTER NOT TO SYSTEMATICALLY DROP THESE TABLES
-----------------------------------------------------------------------------
BEGIN;
drop table addresses cascade; -- can cause lots of issues if not properly dropped
drop table indexer_log cascade; -- should remain small
drop table chain cascade; -- tiny table
ROLLBACK;
END;

-----------------------------------------------------------------------------
-- CHAIN TABLES
-----------------------------------------------------------------------------
BEGIN;
drop table block cascade;
drop table operation cascade;
drop table operation_alpha cascade;
drop table manager_numbers cascade;
drop table implicit cascade;
drop table endorsement cascade;
drop table seed_nonce_revelation cascade;
drop table block_alpha cascade;
drop table deactivated cascade;
drop table contract cascade;
drop table contract_balance cascade;
drop table tx cascade;
drop table origination cascade;
drop table delegation cascade;
drop table balance cascade;
drop table snapshot cascade;
drop table delegated_contract cascade;
-- drop table block_hash cascade;
-- drop table origination_results cascade;
-- drop table delegate cascade;
-- drop table stake cascade;
ROLLBACK;
END;

-----------------------------------------------------------------------------
-- MEMPOOL TABLES
-----------------------------------------------------------------------------
BEGIN;
drop table mempool_op_branch cascade;
drop table mempool_operation_alpha cascade;
ROLLBACK;
END;
