(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Encoders from Tezos types to Caqti (SQL) types.}  *)

(**#X ifdef __META1__
open Tezos_raw_protocol_(**# get PROTO#**)
open Alpha_context
X#**)(**#X else
open Protocol
open Alpha_context
X#**)


(**# ifdef __META3__
module Contract : sig
  include module type of struct include Contract end
  type big_map_diff = unit
  type lazy_storage_diff = Lazy_storage.diffs
end
   #**)
(**# else
module Contract : sig
  include module type of struct include Contract end
  type lazy_storage_diff = unit
end
   #**)


open Caqti_type

val k : Contract.t t
val tez : Tez.t t
val lazy_expr : Script.lazy_expr t
val script : Script.t t
val balance : Delegate.balance t
val balance_update : Delegate.balance_update t
val cycle : Cycle.t t
(**# ifdef __PROTO8__ val voting_period : int32 t #**)(**# else val voting_period : Voting_period.t t #**)
val voting_period_kind : Voting_period.kind t


(** {1 Typed requests for Indexer's SQL DB.} *)

module Block_alpha_table : sig
  val insert :
    ((Block_hash.t * public_key_hash * int32 * Cycle.t) *
     (int32
      (**# ifdef __PROTO8__ * int32 #**)(**# else * Voting_period.voting_period #**)
      * int32 * Voting_period.kind) *
     Fpgas.t, unit, [`Zero]) Caqti_request.t
end

module Operation_alpha_table : sig
  val insert :
    (Operation_hash.t * int * int, unit, [`Zero]) Caqti_request.t
  val update_sender_receiver :
    (Contract.t option * Contract.t option * Operation_hash.t * int, unit, [`Zero]) Caqti_request.t

end

module Manager_numbers : sig
  val insert :
    ((Operation_hash.t * int) * (Z.t * Z.t * Z.t), unit, [`Zero]) Caqti_request.t
end

module Contract_table : sig
  type t = {
    k: Contract.t ;
    bh : Block_hash.t ;
    manager: Signature.Public_key_hash.t (**# ifndef __META1__ option #**) ;
    delegate: Signature.Public_key_hash.t option ;
    spendable: bool option ;
    delegatable: bool option ;
    credit: Tez.t option ;
    preorigination: Contract.t option ;
    script: Script.t option ;
  }

  val sql_encoding : t Caqti_type.t

  val update_script : (Contract.t * Script.t, unit, Caqti_mult.zero) Caqti_request.t
  val get_scriptless_contracts : (unit, Contract.t, Caqti_mult.zero_or_more) Caqti_request.t

  val upsert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val insert_balance : (Contract.t * Block_hash.t * Tez.t, unit, Caqti_mult.zero) Caqti_request.t
  val update_delegate :
    (public_key_hash option * Contract.t * Block_hash.t, unit, [`Zero]) Caqti_request.t
  val upsert_discovered :
    (((Contract.t * Block_hash.t * public_key_hash option) *
      (bool option * bool option * Script.t option)),
     unit, Caqti_mult.zero) Caqti_request.t
end

module Balance_table : sig
  type t = (Delegate.balance * Delegate.balance_update) list

  val update :
    ?op:Operation_hash.t * int ->
    (module Caqti_lwt.CONNECTION) -> bh:Block_hash.t -> t -> unit Lwt.t
end

module Origination_table : sig
  type t = {
    op: Operation_hash.t ;
    op_id: int ;
    src: Contract.t ;
    k: Contract.t ;
    consumed_gas : Fpgas.t ;
    storage_size : Z.t ;
    paid_storage_size_diff : Z.t ;
    big_map_diff : Contract.big_map_diff option ;
    lazy_storage_diff : Contract.lazy_storage_diff option ;
  }

  val insert : (t, unit, [`Zero]) Caqti_request.t

  val select_by_source :
    (Signature.Public_key_hash.t, t, Caqti_mult.zero_or_more) Caqti_request.t
end

module Tx_table : sig
  type t = {
    op : Operation_hash.t ;
    op_id : int ;
    source : Contract.t ;
    destination : Contract.t ;
    fee : Tez.t ;
    amount : Tez.t ;
    parameters : Script.lazy_expr option ;
    storage : Script.lazy_expr option ;
    consumed_gas : Fpgas.t ;
    storage_size : Z.t ;
    paid_storage_size_diff : Z.t ;
    entrypoint : string option;
    big_map_diff :
      Contract.big_map_diff option;
    lazy_storage_diff : Contract.lazy_storage_diff option ;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Delegation_table : sig
  val insert :
    (Operation_hash.t * int * Contract.t *
     public_key_hash option, unit, [`Zero]) Caqti_request.t
end

module Implicit_table2 : sig
  val upsert_reveal :
    (Contract.t * Operation_hash.t * Signature.public_key, unit, [`Zero]) Caqti_request.t
end


module Addresses : sig
  val insert : (Contract.t, unit, [`Zero]) Caqti_request.t
end

module Endorsement_table : sig
  type t = {
    op_hash : Operation_hash.t;
    op_id : int;
    level : int32;
    delegate : Signature.Public_key_hash.t ;
    slots : int list ;
  }
  val insert : (t, unit, [`Zero]) Caqti_request.t
end

module Mempool_operations : sig
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  type t = {
    branch: Block_hash.t;
    ophash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Signature.public_key_hash option;
    destination: Contract.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level: int32;
  }

  val string_of_status : status -> string
  val status_of_string : string -> status option
  val sql_encoding : t Caqti_type.t
  val insert : (t, unit, [ `One | `Zero ]) Caqti_request.t
end

module Seed_nonce_revelation_table : sig
  type t = {
    bh : Block_hash.t;
    op_hash : Operation_hash.t;
    op_id : int;
    level : int32;
    nonce : Nonce.t
  }
  val insert : (t, unit, [`Zero]) Caqti_request.t
end
