(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let proto = "(**# get PROTO #**)"

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X ifdef __META2__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
(**#X ifdef __META3__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)

(* Notes: protocol transition files, including `chain_db.ml`, are generated and
   placed in the `bin_indexer` folder, where they use `bin_indexer/dune`,
   while the other `chain_db.ml` files are placed in protocol-specific
   folders, each having its own `dune` file.
   That's the first reason why transition files require different `open`s.

   Then, with protocol 5, the architecture of the modules changed. That's the
   second reason why `open`s are different for the two categories of protocols
   (before 5, and from 5).
*)

open Alpha_context

open Db_shell
open (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Db_alpha

(**#X ifdef __TRANSITION__
module BS = Block_services.Make(Tezos_protocol_(**# get PROTO#**).Protocol)(Tezos_protocol_(**# get PROTONEXT#**).Protocol)
X#**)(**# elseifdef __META2__
module BS = Block_services.Make(Protocol)(Protocol)
#**)(**# elseifdef __META3__
module BS = Block_services.Make(Protocol)(Protocol)
#**)(**#X else
module BS = Tezos_client_(**# get PROTO #**).Alpha_client_context.Alpha_block_services
X#**)


module Verbose = struct
  include Tezos_indexer_lib.Verbose
  open Utils

  module PP = struct
    type contract = Contract.t
    let contract = Contract.pp
    type tez = Tez.t
    let tez = Tez.pp
    type nonce = Nonce.t
    let nonce out n =
      Format.fprintf out "%a"
        Data_encoding.Json.pp
        (Data_encoding.Json.construct Nonce.encoding n)
  end

  module Debug = DebugF(PP)

  module Log = struct
    module Log = Tezos_indexer_lib.Verbose.Log(PP)
    include Log

    let string_of_voting_period_kind = function
      | Voting_period.Proposal -> "proposal"
      | Testing_vote           -> "testing_vote"
      | Testing                -> "testing"
      | Promotion_vote         -> "promotion_vote"
(**# ifdef __PROTO8__
      | Adoption       -> "adoption"
   #**)

    let balance_update ?force ?op level bhash balance_updates =
      List.iter
        (fun (balance, diff) ->
           let bt, k, cycle =
             match balance with
             | Delegate.Contract k -> "delegate", k, None
             | Rewards (pkh, cycle) -> "rewards", Contract.implicit_contract pkh, Some cycle
             | Fees (pkh, cycle) -> "fees", Contract.implicit_contract pkh, Some cycle
             | Deposits (pkh, cycle) -> "deposits", Contract.implicit_contract pkh, Some cycle
           in
             printf ?force ~vl:4
               "> event=update block_level=%ld block=%a type=balance \
                contract=%a%a balance_kind=%s%a diff=%Ld"
               level
               Block_hash.pp_short bhash
               Contract.pp k
               (fun out -> function
                  | None -> ()
                  | Some (oph, id) ->
                    Format.fprintf out " op_hash=%a id=%d" Operation_hash.pp oph id)
               op
               bt
               (pp_option ~name:"cycle" ~pp:Cycle.pp) cycle
               (match diff with
                | Delegate.Debited v -> Int64.neg (Tez.to_mutez v)
                | Credited v -> Tez.to_mutez v
               )
        )
        balance_updates


  end

end

let cache_size = 100l

let record_address =
  (* Simple cache-based optimisation to avoid putting the same address over and
     over in the address table. The size of this cache is limited by the size of
     [cache_size] blocks: the cache is emptied at each [cache_size] blocks. *)
  let max_level = ref 0l in
  let module S = Set.Make (struct type t = Contract.t let compare = compare end) in
  let set = ref S.empty in
  let add e = set := S.add e !set in
  let mem e = S.mem e !set in
  fun ~__LINE__ level exec k ->
    if level > !max_level  then begin
      max_level := Int32.add cache_size level;
      set := S.empty
    end;
    if mem k then begin
      Verbose.Debug.address_already_recorded_recently ~__LINE__ level k;
      Lwt.return_unit
    end else begin
      add k;
      Verbose.Debug.record_address ~__LINE__ level k;
      exec Addresses.insert k >>= caqti_or_fail >>= fun () ->
      Lwt.return_unit
    end


let record_implicit =
  (* Simple cache-based optimisation to avoid putting the same address over and
     over in the implicit table. The size of this cache is limited by the size of
     [cache_size] blocks: the cache is emptied at each [cache_size] blocks. *)
  let max_level = ref 0l in
  let module S = Set.Make (struct type t = Signature.Public_key_hash.t let compare = compare end) in
  let set = ref S.empty in
  let add e = set := S.add e !set in
  let mem e = S.mem e !set in
  fun ~__LINE__ level bh conn (pkh:Signature.Public_key_hash.t) ->
    if level > !max_level then begin
      max_level := Int32.add cache_size level;
      set := S.empty
    end;
    if mem pkh then begin
      Verbose.Debug.implicit_already_recorded_recently ~__LINE__ level pkh;
      Lwt.return_unit
    end else begin
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      record_address ~__LINE__ level Conn.exec (Contract.implicit_contract pkh) >>= fun () ->
      add pkh;
      Verbose.Log.implicit_insert_discovered level bh pkh;
      Conn.exec Implicit_table.insert_discovered (pkh: Signature.Public_key_hash.t)
      >>= caqti_or_fail >>= fun () ->
      Lwt.return_unit
    end


module BalanceCache = struct
    (**#X ifdef __TRANSITION__
       let contract_fix : Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Contract.t ->
        Tezos_raw_protocol_(**# get PROTONEXT#**).Alpha_context.Contract.t = Obj.magic
     let tez_fix : Tezos_raw_protocol_(**# get PROTONEXT#**).Alpha_context.Tez.t tzresult Lwt.t ->
        Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Tez.t tzresult Lwt.t = Obj.magic
     X#**)
  let get_balance_from_cctxt_no_cache (cctxt:#RPC_context.simple) bh k =
    (**#X ifdef __TRANSITION__
       let k = contract_fix k in
       Tezos_raw_protocol_(**# get PROTONEXT#**).
       X#**)
    Alpha_services.Contract.info
      cctxt ((`Main, `Hash ((bh:Block_hash.t), 0)):Block_services.chain * Block_services.block) k >>=? fun
      { (**#X ifdef __META1__ (**# ifndef __TRANSITION_4_5__ manager = _ ; spendable = _ ; #**) X#**)
        balance ; delegate = _ ; counter = _ ; script = _ ;
      } ->
      return balance
  (**# ifdef __TRANSITION__ |> tez_fix #**)

  module M = Map.Make (Contract)
  let current_block : Block_hash.t option ref = ref None
  let cache = ref M.empty
  let search cctxt bh k =
    try match !current_block with
      | Some h when bh = h ->
        begin match M.find k !cache with
          | Some r ->
            Verbose.Debug.printf ~vl:5 "# reusing balance=%a for k=%a bh=%a" Tez.pp r Contract.pp k Block_hash.pp bh;
            return (r, `Old)
          | None ->
            raise Not_found
      end
      | _ ->
        begin
          current_block := Some bh;
          cache := M.empty;
          get_balance_from_cctxt_no_cache cctxt bh k >>= function
          | Error _ ->
            Verbose.Debug.eprintf "Couldn't get balance for block=%a" Block_hash.pp bh;
            Lwt.fail (Failure "couldn't get balance")
          | Ok balance ->
            cache := M.add k balance !cache;
            return (balance, `New)
        end
    with Not_found ->
      get_balance_from_cctxt_no_cache cctxt bh k >>= function
      | Error _ ->
        Verbose.Debug.eprintf "Couldn't get balance for block=%a" Block_hash.pp bh;
        Lwt.fail (Failure "couldn't get balance")
      | Ok balance ->
        cache := M.add k balance !cache;
        return (balance, `New)
end

let get_balance_from_cctxt (cctxt:#RPC_context.simple) bh k =
  BalanceCache.search cctxt bh k


let record_contract_balance conn ~cctxt ~bh ~level ~k : _ Lwt.t =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  get_balance_from_cctxt cctxt bh k >>= function
  | Ok (balance, `New) ->
    record_address ~__LINE__ level Conn.exec k >>= fun () ->
    Verbose.Log.contract_update_balance ~bh ~level ~k ~balance ();
    Conn.exec Contract_table.insert_balance (k, bh, balance)
    >>= caqti_or_fail >>= fun () ->
    Lwt.return_unit
  | Ok (_, `Old) ->
    (* already recorded *) (* TODO: debug log *)
    Lwt.return_unit
  | Error _ ->
    Verbose.Debug.eprintf "Couldn't get balance for block=%a" Block_hash.pp bh;
    Lwt.fail (Failure "couldn't get balance")

let store_block =
  let open Apply_results in
  fun conn bh
    ({ shell ; protocol_data = _ } : BS.raw_block_header)
    ({ protocol_data =
         { baker;
           level = ({ level_position;
                      cycle; cycle_position;
                      voting_period;
                      voting_period_position;
                      level;
                      expected_commitment = _;
                    } (* : Alpha_context.Level.t *));
           (**# ifdef __PROTO8__ level_info = _; #**)
           (**# ifdef __PROTO8__ voting_period_info = _; #**)
           voting_period_kind;
           consumed_gas ;
           deactivated ;
           balance_updates ;
           nonce_hash = _;};
       test_chain_status = _;
       max_operations_ttl = _;
       max_operation_data_length = _;
       max_block_header_length = _;
       operation_list_quota = _;
     } : BS.block_metadata) ->
    (**# ifndef __MILLIGAS__
    let consumed_gas = Fpgas.t_of_z consumed_gas in
    #**)
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    (* Store shell header *)
    begin (* logged *)
      Verbose.Log.shell bh shell;
      Conn.exec Block_table.insert (bh, shell)
    end >>= caqti_or_fail >>= fun () ->
    (* Discover deactivated pkhs and fill up deactivated table *)
    begin (* self logged *)
      Lwt_list.iter_s (fun pkh ->
          record_implicit ~__LINE__ (Raw_level.to_int32 level) bh conn pkh
        ) deactivated
    end >>= fun () ->
    begin (* logged *)
      let data = List.map (fun pkh ->
          Verbose.Log.deactivated_delegate_table ~block_level:shell.level bh pkh;
          pkh, bh)
          deactivated in
      with_transaction conn Deactivated_delegate_table.insert data
    end >>= fun () ->
    (* Store alpha header *)
    begin (* logged *)
      Verbose.Log.store_alpha_header bh shell.level baker level_position
        (Cycle.to_int32 cycle) cycle_position
        (* Voting_period.pp *) (fun out -> Format.fprintf out "%ld") ((**# ifndef __PROTO8__ Voting_period.to_int32#**) voting_period)
        voting_period_position
        (Verbose.Log.string_of_voting_period_kind voting_period_kind)
        ((**# ifdef __PROTO8__ Fpgas.p8_to_p7 #**) consumed_gas);
      Conn.exec Block_alpha_table.insert
        ((bh, baker, level_position, cycle),
         (cycle_position, voting_period, voting_period_position, voting_period_kind),
         (**# ifdef __PROTO8__ Fpgas.p8_to_p7 #**) consumed_gas)
    end >>= caqti_or_fail >>= fun () ->
    (* Update balances tables *)
    begin (* logged *)
      Verbose.Log.balance_update shell.level bh balance_updates;
      Balance_table.update conn ~bh balance_updates
    end >>= fun () ->
    return_unit


let int_of_contents : type a. a contents -> int = function
  | Endorsement _ -> 0
  | Seed_nonce_revelation _ -> 1
  | Double_endorsement_evidence _ -> 2
  | Double_baking_evidence _ -> 3
  | Activate_account _ -> 4
  | Proposals _ -> 5
  | Ballot _ -> 6
  | Manager_operation { operation; _ } ->
    match operation with
    | Reveal _ -> 7
    | Transaction _ -> 8
    | Origination _ -> 9
    | Delegation _ -> 10

let string_of_contents : type a. a contents -> string = function
  | Endorsement _ -> "Endorsement"
  | Seed_nonce_revelation _ -> "Seed_nonce_revelation"
  | Double_endorsement_evidence _ -> "Double_endorsement_evidence"
  | Double_baking_evidence _ -> "Double_baking_evidence"
  | Activate_account _ -> "Activate_account"
  | Proposals _ -> "Proposals"
  | Ballot _ -> "Ballot"
  | Manager_operation { operation; _ } ->
    match operation with
    | Reveal _ -> "Reveal"
    | Transaction _ -> "Transaction"
    | Origination _ -> "Origination"
    | Delegation _ -> "Delegation"


let update_contract conn (cctxt:#RPC_context.simple) level k bh = (* logged *)
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  record_address ~__LINE__ level Conn.exec k >>= fun () ->
  let spendable = None
  and delegatable = None in
  begin (* self logged *)
    record_contract_balance conn ~cctxt ~bh ~level ~k
  end >>= fun () ->
  begin match Contract.is_implicit k with
    | None -> (* originated *)
      begin (* logged *)
        Verbose.Log.contract_upsert level bh ~subtype:"update-origination" ~spendable ~delegatable ~k ();
        Conn.exec Contract_table.upsert_discovered
          ((k, bh, None), (spendable, delegatable, None))
      end >>= caqti_or_fail >>= fun _ ->
      return_unit
    | Some pkh -> (* implicit account *)
      begin (* self logged *)
        record_implicit ~__LINE__ level bh conn pkh
      end >>= fun () ->
      begin (* self logged *)
        record_contract_balance conn ~cctxt ~bh ~level:level ~k:(Contract.implicit_contract pkh)
      end >>= fun () ->
      begin (* logged *)
        Verbose.Log.contract_upsert level bh ~subtype:"update-implicit" ~spendable ~delegatable ~pkh ~k ();
        Conn.exec Contract_table.upsert_discovered
          ((k, bh, Some pkh), (spendable, delegatable, None))
      end >>= caqti_or_fail >>= fun _ -> return_unit
  end >>= fun _ ->
  return_unit

let operation_alpha_update exec block_level bh ~subtype ~sender ~receiver ~op_hash ~op_id =
  Verbose.Log.oa_update_sender_receiver block_level bh ~subtype ~sender ~receiver op_hash op_id;
  exec Operation_alpha_table.update_sender_receiver
    (sender, receiver, op_hash, op_id)
  >>= caqti_or_fail

let process_mgr_operation :
  type a b.
  (module Caqti_lwt.CONNECTION) ->
  _ ->
  block_level:int32 ->
  Block_hash.t -> Operation_hash.t -> int -> Contract.t -> Tez.t ->
  a manager_operation ->
  b Apply_results.successful_manager_operation_result -> unit tzresult Lwt.t =
  fun conn (cctxt:#RPC_context.simple)
    ~block_level
    bh op_hash op_id source fee operation operation_result ->
    Verbose.Log.process_mgr_operation block_level bh op_hash op_id source fee;
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let process_tx
        (**# ifdef __BIG_MAP_DIFF__ ~big_map_diff #**)
        (**# ifdef __META2__ ?entrypoint #**)
        (**# ifdef __META3__ ~lazy_storage_diff ?entrypoint #**)
        ~consumed_gas ~storage_size ~paid_storage_size_diff
        ~amount ~destination ~balance_updates
        ~originated_contracts ~parameters
        ~storage =
      let spendable = None
      and delegatable = None in
      (* Add discovered contracts *)
      begin (* logged *)
        let manager = None
        and script = None in
        Lwt_list.iter_s
          (fun k ->
             Verbose.Log.contract_upsert block_level bh ~subtype:"discovered"
               ~script ~spendable ~delegatable ~k ();
             record_address ~__LINE__ block_level Conn.exec k >>= fun () ->
             begin (* self logged *)
               record_contract_balance conn ~cctxt ~bh ~level:block_level ~k
             end >>= fun () ->
             Conn.exec Contract_table.upsert_discovered
               ((k, bh, manager), (spendable, delegatable, script))
             >>= caqti_or_fail >>= fun _ ->
             Lwt.return_unit
          )
          originated_contracts
      end >>= fun _ ->
      begin (* self logged *)
      (* Add src and dst *)
        update_contract conn cctxt block_level source bh >>=? fun () ->
        update_contract conn cctxt block_level destination bh
      end >>=? fun () ->
      (* Update balances *)
      begin (* logged *)
        Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
        Balance_table.update
          ~op:(op_hash, op_id) conn ~bh balance_updates
      end >>= fun () ->
      begin (* logged *)
        (* Store TX (or update but does NOT overwrite existing data), key is (op_hash, op_id) *)
        (**# ifdef __META1__ let entrypoint = None in #**)
        (**# ifndef __BIG_MAP_DIFF__ let big_map_diff = None in #**)
        (**# ifndef __STORAGE_DIFF__ let lazy_storage_diff = None in #**)
        Verbose.Log.store_tx block_level bh op_hash op_id
          ~source ~destination fee amount parameters
          storage consumed_gas storage_size paid_storage_size_diff entrypoint
          (* FIXME: lazy_storage_diff *) big_map_diff;
        Conn.find_opt Tx_table.insert
          Tx_table.{ op = op_hash ; op_id ; source ; destination ;
                     fee ; amount ; parameters ; storage ;
                     consumed_gas ; storage_size ; paid_storage_size_diff ;
                     entrypoint ;
                     big_map_diff ;
                     lazy_storage_diff ;
                   }
      end >>= caqti_or_fail >>= fun _ ->
      return_unit in
    let process_origination
        ~delegate (**# ifdef __META1__ ~manager ~spendable ~delegatable #**)
        credit preorigination script balance_updates originated_contracts
        (**# ifdef __META2__ ~big_map_diff #**)
        (**# ifdef __META3__ ~lazy_storage_diff #**)
        ~consumed_gas ~storage_size ~paid_storage_size_diff
      =
      assert (List.length originated_contracts = 1) ;
      (**# ifdef __META2__ let spendable = None and delegatable = None and manager = None in  #**)
      (**# elseifdef __META3__ let spendable = None and delegatable = None and manager = None in  #**)
      (**# else let spendable = Some spendable and delegatable = Some delegatable in  #**)
      let originated_contract = List.hd originated_contracts in
      (* Add discovered implicits *)
      (**# ifdef __META1__
          begin (* self logged *)
            record_implicit ~__LINE__ block_level bh conn manager
          end >>= fun () ->
      #**)
      begin match delegate with
        | None -> Lwt.return_unit
        | Some delegate ->
          begin (* self logged *)
            record_implicit ~__LINE__ block_level bh conn delegate
          end
      end >>= fun () ->
      (* Insert full contract row *)
      record_address ~__LINE__ block_level Conn.exec originated_contract >>= fun () ->
      (**# ifdef __META2__ begin match manager with
         | None -> Lwt.return_unit
         | Some manager -> record_address ~__LINE__ block_level Conn.exec (Contract.implicit_contract manager)
         end >>= fun () -> #**)
      (**# ifdef __META3__ begin match manager with
         | None -> Lwt.return_unit
         | Some manager -> record_address ~__LINE__ block_level Conn.exec (Contract.implicit_contract manager)
         end >>= fun () -> #**)
      begin (* logged *)
        Verbose.Log.contract_upsert block_level bh
          (**# ifdef __META1__ ~manager #**)
          (**# else ?manager #**)
          ~subtype:"origination"
          ?script ~spendable ~delegatable ~credit ?preorigination ~k:(originated_contract:Contract.t) ();
        begin (* self logged *)
          record_contract_balance conn ~cctxt ~bh ~level:block_level ~k:originated_contract
        end >>= fun () ->
        Conn.find_opt Contract_table.upsert
          { k = originated_contract ; bh ; manager ; delegate ; spendable ;
            delegatable ; credit = Some credit ; preorigination ; script }
      end >>= caqti_or_fail >>= fun _ ->
      (* Insert in origination table *)
      begin (* logged *)
        Verbose.Log.insert_origination block_level bh op_hash op_id source originated_contract;
        Conn.exec Origination_table.insert {
          op = op_hash ; op_id ; src = source ;
          k = originated_contract ;
          consumed_gas ; storage_size ; paid_storage_size_diff ;
          big_map_diff (**# ifndef __META2__ = None #**) ;
          lazy_storage_diff (**# ifndef __PROTO8__ = None #**) ;
        }
      end >>= caqti_or_fail >>= fun () ->
      (* Update balances *)
      begin (* logged *)
        Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
        Balance_table.update
          ~op:(op_hash, op_id) conn ~bh balance_updates
      end >>= fun () ->
      begin (* self logged *)
        operation_alpha_update Conn.exec block_level bh ~subtype:"origination"
          ~sender:(Some source) ~receiver:None ~op_hash ~op_id
      end >>= fun () ->
      Verbose.Debug.store_origination block_level bh source;
      return_unit
    in
    match operation, operation_result with
    | Transaction { amount ; destination ; parameters ;
                    (**# ifdef __META2__ entrypoint #**)
                    (**# ifdef __META3__ entrypoint #**) },
      Apply_results.Transaction_result
        { balance_updates ; originated_contracts ; storage ;
          (**# ifdef __BIG_MAP_DIFF__ big_map_diff ; #**)
          (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
          consumed_gas ; storage_size ; paid_storage_size_diff ;
          (**# ifdef __PROTO1__ #**)
          (**# elseifdef __PROTO2__ #**)
          (**# else allocated_destination_contract = (true|false) ; #**)
        } ->
      (**# ifndef __MILLIGAS__
         let consumed_gas = Fpgas.t_of_z consumed_gas in
         #**)
      (* update sender and receiver in operation_alpha table *)
      begin
        if source <> destination then
          record_address ~__LINE__ block_level Conn.exec destination
        else Lwt.return_unit
      end >>= fun () ->
      begin (* self logged *)
        operation_alpha_update Conn.exec block_level bh ~subtype:"transaction"
          ~sender:(Some source) ~receiver:(Some destination) ~op_hash ~op_id
      end >>= fun () ->
      (**# ifndef __META1__ let parameters = Some parameters in #**)
      let storage =
        match storage with
        | Some storage -> Some (Script.lazy_expr storage)
        | None -> None in
      begin (* self logged *)
        process_tx
          (**# ifdef __BIG_MAP_DIFF__ ~big_map_diff #**)
          (**# ifdef __STORAGE_DIFF__ ~lazy_storage_diff #**)
          ~consumed_gas(**# ifdef __PROTO8__ :(Fpgas.p8_to_p7 consumed_gas) #**)
          ~storage_size ~paid_storage_size_diff
          ~amount ~destination
          (**# ifdef __META2__ ~entrypoint #**)(**# ifdef __META3__ ~entrypoint #**)
          ~balance_updates ~originated_contracts ~parameters
          ~storage
      end
    | Reveal pk, _ ->
      begin (* self logged *)
        (* update sender and receiver in operation_alpha table *)
        operation_alpha_update Conn.exec block_level bh ~subtype:"reveal"
          ~sender:(Some source) ~receiver:None ~op_hash ~op_id
      end >>= fun () ->
      begin (* logged *)
        Verbose.Log.store_revelation block_level bh pk;
        Conn.exec Implicit_table2.upsert_reveal
          (source, op_hash, pk) >>=
        caqti_or_fail >>= fun () ->
        return_unit
      end
    | Origination { (**# ifdef __META1__ manager; spendable; delegatable; #**)
        delegate; credit; preorigination ; script },
      Apply_results.Origination_result { balance_updates ;
                                         originated_contracts ;
                                         (* FIXME MAYBE: the following fields are not indexed *)
                                         (**# ifdef __META2__ big_map_diff ; #**)
                                         (**# ifdef __META3__ lazy_storage_diff ; #**)
                                         consumed_gas ;
                                         storage_size ;
                                         paid_storage_size_diff ;
                                       } ->
      (**# ifdef __META2__ let script = Some script in #**)
      (**# ifdef __META3__ let script = Some script in #**)
      (**# ifndef __MILLIGAS__
         let consumed_gas = Fpgas.t_of_z consumed_gas in
         #**)
      begin (* self logged *)
        operation_alpha_update Conn.exec block_level bh ~subtype:"origination"
          ~sender:(Some source) ~receiver:None ~op_hash ~op_id
      end >>= fun () ->
      begin (* self logged *)
        process_origination
          ~delegate (**# ifdef __META1__ ~manager ~spendable ~delegatable #**)
          credit preorigination script balance_updates originated_contracts
          (**# ifdef __META2__ ~big_map_diff #**)
          (**# ifdef __META3__ ~lazy_storage_diff #**)
          ~consumed_gas(**# ifdef __PROTO8__ :(Fpgas.p8_to_p7 consumed_gas) #**)
          ~storage_size ~paid_storage_size_diff
      end
    | Delegation maybe_pkh, _ ->
      (* update sender and receiver in operation_alpha table *)
      begin
        let receiver =
          (match maybe_pkh with
           | Some pkh -> Some (Contract.implicit_contract pkh)
           | None -> None
          )
        in
        begin match receiver with
          | None -> Lwt.return_unit
          | Some r -> record_address ~__LINE__ block_level Conn.exec r
        end >>= fun () ->
        begin (* self logged *)
          operation_alpha_update Conn.exec block_level bh ~subtype:"delegation"
            ~sender:(Some source) ~receiver ~op_hash ~op_id
        end
      end >>= fun () ->
      begin (* logged *)
        Verbose.Log.add_delegation block_level bh source maybe_pkh;
        Conn.exec Delegation_table.insert (op_hash, op_id, source, maybe_pkh)
      end >>= caqti_or_fail >>= fun () ->
      begin (* self logged *)
        record_contract_balance conn ~cctxt ~bh ~level:block_level ~k:source
      end >>= fun () ->
      begin (* logged *)
        Verbose.Log.update_contract_delegate block_level bh source maybe_pkh;
        Conn.exec Contract_table.update_delegate (maybe_pkh, source, bh)
      end >>= caqti_or_fail >>= fun () ->
      return_unit
    | _, _ -> assert false

let store_op_id :
  type a b.
  (module Caqti_lwt.CONNECTION) ->
  _ ->
  block_level:int32 ->
  Operation_hash.t -> int -> Block_hash.t ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun conn (cctxt:#RPC_context.simple) ~block_level op_hash op_id bh contents meta ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  begin (* logged *)
    Verbose.Log.operation_alpha_preinsert block_level bh op_hash op_id (string_of_contents contents);
    Conn.exec Operation_alpha_table.insert
      (op_hash, op_id, int_of_contents contents)
  end >>= caqti_or_fail >>= fun () ->
  match contents, meta with
  | Seed_nonce_revelation {level ; nonce},
    Apply_results.Seed_nonce_revelation_result balance_updates ->
    let level = Raw_level.to_int32 level in
    begin (* logged *)
      Verbose.Log.balance_update
        ~op:(op_hash, op_id) block_level bh balance_updates;
      Balance_table.update conn ~bh ~op:(op_hash, op_id) balance_updates
    end >>= fun () ->
    begin (* logged *)
      Verbose.Log.seed_nonce_revelation block_level bh op_hash op_id ~level
        ~nonce;
      Conn.exec Seed_nonce_revelation_table.insert
        { bh; op_hash; op_id; level; nonce }
    end
    >>= caqti_or_fail >>= fun _ -> return_unit
  | Double_endorsement_evidence { op1 = _ ; op2 = _},
    Apply_results.Double_endorsement_evidence_result balance_updates ->
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
      Balance_table.update conn ~bh ~op:(op_hash, op_id) balance_updates
    end >>= fun () -> return_unit
  | Double_baking_evidence { bh1 = _ ; bh2 = _ },
    Apply_results.Double_baking_evidence_result balance_updates ->
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
      Balance_table.update conn ~bh ~op:(op_hash, op_id) balance_updates
    end >>= fun () -> return_unit
  | Activate_account { id; activation_code = _; },
    Apply_results.Activate_account_result balance_updates ->
    begin
      let k = Contract.implicit_contract (Ed25519 id) in
      record_address ~__LINE__ block_level Conn.exec k >>= fun () ->
      begin (* self logged *)
        record_contract_balance conn ~cctxt ~bh ~level:block_level ~k
      end >>= fun () ->
      let sender = Some k
      and receiver = None in
      begin (* self logged *)
        operation_alpha_update Conn.exec block_level bh ~subtype:"activate" ~sender ~receiver ~op_hash ~op_id
      end >>= fun () ->
      (* Add implicit *)
      begin (* logged *)
        Verbose.Log.upsert_activated block_level bh (Ed25519 id) op_hash;
        Conn.exec Implicit_table.upsert_activated (Ed25519 id, op_hash)
      end >>= caqti_or_fail >>= fun () ->
      (* Add contract *)
      let script = None in
      Verbose.Debug.printf ~vl:3 "# add contract using Contract_table.upsert_discovered %s" __LOC__;
      let spendable = None
      and delegatable = None in
      begin (* logged *)
        Verbose.Log.contract_upsert block_level bh ~subtype:"activate_account"
          ~spendable ~delegatable ~k ~script ();
        Conn.exec Contract_table.upsert_discovered
          ((k, bh, None), (spendable, delegatable, script))
      end >>=
      caqti_or_fail >>= fun _ ->
      (* Update balances *)
      begin (* logged *)
        Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
        Balance_table.update conn ~bh ~op:(op_hash, op_id) balance_updates
      end >>= fun () ->
      return_unit
    end
  | Endorsement { level },
    Apply_results.Endorsement_result { balance_updates ;
                                       delegate ;
                                       slots } ->
    (* Update endorsements *)
    let level = Raw_level.to_int32 level in
    begin (* logged *)
      Verbose.Log.endorsement_result block_level bh
        op_hash op_id ~level ~delegate ~slots;
      Conn.exec Endorsement_table.insert
        { op_hash; op_id; level; delegate; slots }
    end >>= caqti_or_fail >>= fun _ ->
    let sender = Some(Contract.implicit_contract delegate)
    and receiver = None in
    begin (* self logged *)
      operation_alpha_update Conn.exec block_level bh ~subtype:"endorsement"
        ~sender ~receiver ~op_hash ~op_id
    end >>= fun () ->
    (* upsert balance in contract table *)
    begin (* self logged *)
      record_contract_balance conn ~cctxt ~bh ~level:block_level
        ~k:(Contract.implicit_contract delegate)
    end >>= fun () ->
    (* Update balances *)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
      Balance_table.update ~op:(op_hash, op_id) conn ~bh balance_updates
    end >>= fun () ->
    return_unit
  | Manager_operation { source; fee; operation; gas_limit; storage_limit ; counter},
    Apply_results.Manager_operation_result {
      balance_updates ; (* Correspond to the operation fees *)
      operation_result = Applied operation_result; (* matched and processed later *)
      internal_operation_results ; (* matched and processed later *)
    } ->
    (**# ifdef __MILLIGAS__
    let gas_limit = Gas.Arith.integral_to_z gas_limit in
       #**)
    begin (* logged *)
      Verbose.Log.manager_numbers block_level bh ~op_hash ~op_id ~counter ~gas_limit ~storage_limit;
      Conn.exec Manager_numbers.insert ((op_hash, op_id), (counter, gas_limit, storage_limit))
    end >>= caqti_or_fail >>= fun () ->
    let process_internal_op_results a =
      iter_s
        (function
          | Apply_results.Internal_operation_result
              ({ source; operation; nonce = _ }, Applied operation_result) ->
            begin (* self logged *)
              process_mgr_operation conn cctxt
                ~block_level
                bh op_hash op_id
                source fee operation operation_result
            end
          | Apply_results.Internal_operation_result
              ({ source; operation = _ ; nonce = _ }, _) ->
            begin (* self logged *)
              operation_alpha_update Conn.exec block_level bh ~subtype:"failed_operation"
                ~sender:(Some source) ~receiver:None ~op_hash ~op_id
                (* Register but do not process effects of failed operations *)
                (* FIXME: index failed operations *)
            end >>= fun () ->
            return_unit
        )
        a
    in
    begin (* self logged *)
      process_internal_op_results internal_operation_results
    end >>=? fun () ->
    (**# ifdef __META2__ let source = (Contract.implicit_contract source) in #**)
    (**# ifdef __META3__ let source = (Contract.implicit_contract source) in #**)
    begin (* self logged *)
      process_mgr_operation conn cctxt
        ~block_level
        bh op_hash op_id
        source fee operation operation_result
    end >>=? fun () ->
    (* Update balances (global operation fee) *)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id) block_level bh balance_updates;
      Balance_table.update conn ~bh ~op:(op_hash, op_id)
        balance_updates
    end >>= fun () ->
    return_unit
  | Manager_operation { source ; _ },
    Manager_operation_result {operation_result=Backtracked (_, _); _ }
  | Manager_operation { source ; _ },
    Manager_operation_result {operation_result=Failed (_, _); _ }
  | Manager_operation { source ; _ },
    Manager_operation_result {operation_result=Skipped _; _ }
  | Manager_operation { source ; _ }, Proposals_result
  | Manager_operation { source ; _ }, Ballot_result
  | Manager_operation { source ; _ }, Endorsement_result _
  | Manager_operation { source ; _ }, Seed_nonce_revelation_result _
  | Manager_operation { source ; _ }, Double_endorsement_evidence_result _
  | Manager_operation { source ; _ }, Double_baking_evidence_result _
  | Manager_operation { source ; _ }, Activate_account_result _
    ->
    (**# ifdef __META2__ let source = Contract.implicit_contract source in #**)
    (**# ifdef __META3__ let source = Contract.implicit_contract source in #**)
    begin (* self logged *)
      (* FIXME: TODO: index failed operations *)
      operation_alpha_update Conn.exec block_level bh ~subtype:"failed_operation"
        ~sender:(Some source) ~receiver:None ~op_hash ~op_id
    end >>= fun () ->
    (* failed/skipped/backtracked manager operation, ignore. *)
    return_unit
  | Endorsement _, (
      Proposals_result
    |  Ballot_result
    |  Seed_nonce_revelation_result _
    |  Double_endorsement_evidence_result _
    |  Double_baking_evidence_result _
    |  Activate_account_result _
    |  Manager_operation_result _)
  | Activate_account _, (
      Ballot_result
    | Double_baking_evidence_result _
    | Double_endorsement_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Proposals_result
    | Seed_nonce_revelation_result _
    )
  | Double_baking_evidence _, (
      Proposals_result
    | Ballot_result
    | Double_endorsement_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Seed_nonce_revelation_result _
    | Activate_account_result _
    )
  | Double_endorsement_evidence _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Seed_nonce_revelation_result _
    | Activate_account_result _
    )
  | Seed_nonce_revelation _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Double_endorsement_evidence_result _
    )
  | Proposals _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Double_endorsement_evidence_result _
    | Seed_nonce_revelation_result _
    )
  | Ballot _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Double_endorsement_evidence_result _
    | Seed_nonce_revelation_result _
    )
    ->
    assert false


let store_op conn (cctxt:#RPC_context.simple) ~block_level bh
    ({ BS.chain_id ; hash = op_hash ;
       protocol_data = Operation_data { contents ; signature = _ } ;
       receipt ; shell = _ }) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let rec inner :
    type a. int -> a contents_list ->
    _ -> unit tzresult Lwt.t =
    fun op_id contents receipt ->
      (* match receipt with
       * | None -> assert false
       * | Some receipt -> *)
        match contents, receipt with
        | Single _, Apply_results.No_operation_metadata ->
          assert false
        | Cons _, Apply_results.No_operation_metadata ->
          assert false
        | Single _, Apply_results.Operation_metadata { contents = Apply_results.Cons_result _ } ->
          assert false
        | Cons _, Apply_results.Operation_metadata { contents = Apply_results.Single_result _} ->
          assert false
        | Single op,
          Apply_results.Operation_metadata { contents = Apply_results.Single_result a } ->
          store_op_id conn cctxt ~block_level op_hash op_id bh op a
        | Cons (op, rest),
          Apply_results.Operation_metadata { contents = Apply_results.Cons_result (a, meta)} ->
          store_op_id conn cctxt ~block_level op_hash op_id bh op a >>=? fun () ->
          inner (succ op_id) rest (Apply_results.Operation_metadata { contents = meta })
  in
  begin (* logged *)
    Verbose.Log.store_raw_operation block_level bh op_hash;
    Conn.exec Operation_table.insert (op_hash, chain_id, bh)
  end >>=
  caqti_or_fail >>= fun () ->
  match receipt with
  | None -> assert false (* FIXME: TODO: PROTO 8 *)
  | Some receipt ->
    inner 0 contents receipt

let store_ops db (cctxt:#RPC_context.simple) ~block_level bh ops =
  match ops with
  (* FIXME: treat voting *)
  | [endorsements; _voting; anon; manager] ->
    begin (* self logged *)
      iter_s (store_op db cctxt ~block_level bh) endorsements >>=? fun () ->
      iter_s (store_op db cctxt ~block_level bh) anon >>=? fun () ->
      iter_s (store_op db cctxt ~block_level bh) manager
    end
  | _ -> assert false

let previous_chain_id = ref None


let store_block_full ?chain ?block (cctxt:#RPC_context.simple) conn =
  (* let _ : #RPC_context.simple = cctxt in *)
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.start () >>= caqti_or_fail >>= fun () ->
  Verbose.Debug.transaction_start ();
  BS.info cctxt ?chain ?block () >>=? fun
    { chain_id ; hash ; header = { shell ; protocol_data = _ } as header ;
      metadata ; operations } ->
  (* Store chain *)
  (if Some chain_id = !previous_chain_id then
     Lwt.return (Ok ())
   else
     let () = previous_chain_id := Some chain_id in
     Conn.exec Chain_id_table.insert chain_id) >>=
  caqti_or_fail >>= fun () ->
  (* Store block header (shell + alpha) *)
  match metadata with
  | None -> assert false (* FIXME: TODO: PROTO 8 *)
  | Some metadata ->
    store_block conn hash header metadata >>=? fun () ->
    (* Store ops (shell + alpha) *)
    store_ops conn cctxt ~block_level:shell.level hash operations >>=? fun () ->
    Conn.commit () >>= caqti_or_fail >>= fun () ->
    Verbose.Debug.transaction_committed ();
    Verbose.Debug.block_stored hash shell.level;
    return_unit


let discover_initial_ks (cctxt:#RPC_context.simple) blockid conn =
  (* When the chain starts being indexed, we need to become aware of
     all existing contracts.  *)
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  BS.info cctxt
    ~chain:(fst blockid) ~block:(snd blockid) () >>=?
  fun { chain_id; hash = bh; header = { shell ; protocol_data = _ };
        metadata = _;  operations = _ } ->
  let block_level = shell.level in
  Alpha_services.Contract.list cctxt blockid >>=? fun ks ->
  iter_s (fun k ->
    Alpha_services.Contract.info
      cctxt blockid k >>=? fun
      { (**# ifdef __META1__ manager; spendable; #**)
        balance ; delegate ; counter = _ (* FIXME *) ; script ;
      }
        ->
(**# ifdef __META1__
    let spendable = Some spendable in
    (* add implicit *)
   begin (* self logged *)
    record_implicit ~__LINE__ block_level bh conn manager
   end >>= fun () ->
   #**)
(**# else let spendable = None in
    (* add implicit *)
    begin
      match delegate with None -> Lwt.return_unit
      | Some delegate ->
        begin (* self logged *)
          record_implicit ~__LINE__ block_level bh conn delegate
        end
    end >>= fun () ->
#**)
(**# ifdef __META1__ let delegatable = Some (fst delegate) in #**)
(**# else let delegatable = Some (Contract.is_implicit k = None) in #**)
    (* add chain *)
    begin (* FIXME: log me *)
      Conn.exec Chain_id_table.insert chain_id
    end >>= caqti_or_fail >>= fun () ->
    (* add block header *)
    begin (* FIXME : log me *)
      Conn.exec Block_table.insert (bh, shell)
    end >>= caqti_or_fail >>= fun () ->
    begin (* logged *)
      record_address ~__LINE__ block_level Conn.exec k >>= fun () ->
      Verbose.Log.contract_update_balance ~bh ~level:block_level ~k ~balance ();
      Conn.exec Contract_table.insert_balance (k, bh, balance)
    end >>= caqti_or_fail >>= fun () ->
    (* add contract *)
    begin (* logged *)
      Verbose.Log.contract_upsert block_level bh ~subtype:"discovered-contract"
        ~spendable ~delegatable ~k (**# ifdef __META1__ ~manager #**)
        ?delegate(**# ifdef __META1__ :(snd delegate) #**)
        ~credit:balance
        ~script
        ();
      Conn.find_opt
        Contract_table.upsert
        { k ;
          bh ;
          manager (**# ifdef __META2__ = None #**)(**# ifdef __META3__ = None #**) ;
          delegate (**# ifdef __META1__ = snd delegate #**) ;
          spendable ;
          delegatable ;
          credit = Some balance ;
          preorigination = None ;
          script
        }
    end >>= caqti_or_fail >>= fun (Some () | None) ->
    return_unit
    )
    ks
  >>=? fun () ->
  return_unit

let bootstrap_generic
    ?(blocks_per_sql_tx=300l) ?from ?up_to ~first_alpha_level (cctxt:#RPC_context.simple) conn
    ~(f:(**## ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full ->##**)(**# else _ ->#**)
        (module Caqti_lwt.CONNECTION) ->
      int32 -> unit tzresult Lwt.t) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.find Block_table.select_max_level () >>=
  caqti_or_fail >>= fun level ->
  let from =
    match from, level with
    | Some lvl, _ -> lvl
    | None, None -> first_alpha_level
    | None, Some lvl -> Int32.succ lvl in
  begin match level with
    | Some _ -> return_unit
    | None ->
      Conn.start () >>= caqti_or_fail >>= fun () ->
      Verbose.Debug.transaction_start ();
      discover_initial_ks cctxt (`Main, `Level first_alpha_level) conn
      >>= fun _ ->
      Conn.commit () >>= caqti_or_fail >>= fun () ->
      Verbose.Debug.transaction_committed ();
      Lwt.return (Ok())
  end >>=? fun () ->
  Verbose.Log.start_downloading_chain from;
  let rec process_n_blocks conn counter lvl =
    match counter, up_to with
    | c, _ when c <= 0l -> return (`Counter_exhausted, lvl)
    | _, Some target when lvl > target ->
      Verbose.Log.bootstrapping_target_reached target;
      return (`Target_reached, lvl)
    | _ ->
      Verbose.Log.processing_block lvl;
      protect begin fun () ->
        f cctxt conn lvl >>=? fun () ->
        return `Reached
      end ~on_error:begin function
        | [RPC_context.Not_found _] ->
          return `Reached_head
        | e ->
          Verbose.eprintf "%a" pp_print_error e;
          Lwt.return (Error e)
      end >>=? function
      | `Reached ->
        process_n_blocks conn (Int32.pred counter) (Int32.succ lvl)
      | `Reached_head ->
        return (`Reached_head, lvl)
  in
  let rec inner lvl =
    let blocks_to_download =
      match up_to with
      | None -> blocks_per_sql_tx
      | Some up_to ->
        min (Int32.sub up_to lvl) blocks_per_sql_tx in
    if blocks_to_download <= 0l then begin
      Verbose.Log.bootstrapping_done proto;
      return lvl
    end else
      process_n_blocks conn blocks_to_download lvl >>=?
      fun (status, lvl) ->
      match status with
      | `Reached_head -> return lvl
      | `Target_reached -> return lvl
      | `Counter_exhausted -> inner lvl
  in
  inner from

let bootstrap_chain ?from ?up_to ~first_alpha_level (cctxt:#RPC_context.simple) db =
  match up_to with
  | Some n when	n < 2l ->
    return n
  | _ ->
    let f cctxt db lvl =
      store_block_full ~block:(`Level lvl) cctxt db
    in
    bootstrap_generic ?from ?up_to ~first_alpha_level cctxt db ~f
