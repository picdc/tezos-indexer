(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(**#X ifdef __META1__
open Tezos_raw_protocol_(**# get PROTO#**)
X#**)(**# else
open Protocol
#**)
open Alpha_context


(**# ifdef __META3__
module Contract = struct
  include Contract
  type big_map_diff = unit
  type lazy_storage_diff = Lazy_storage.diffs
end
#**)
(**# else
module Contract = struct
   include Contract
   type lazy_storage_diff = unit
end
#**)


open Caqti_type.Std
open Db_shell

let to_caqti_error v =
  match (**#X ifdef __META1__ Tezos_protocol_environment_(**# get PROTO#**).X#**)Environment.wrap_error v with
  | Ok a -> Ok a
  | Error e -> Error (Format.asprintf "%a" pp_print_error e)

let cycle =
  let open Cycle in
  custom
    ~encode:(fun a -> Ok (to_int32 a))
    ~decode:(fun a -> Ok (add root (Int32.to_int a)))
    int32

(**# ifdef __PROTO8__
let voting_period = int32
   #**)
(**# else
let voting_period =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct Voting_period.encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct Voting_period.encoding a))
    json
   #**)

let voting_period_kind =
  let open Voting_period in
  custom
    ~encode:(function
        | Proposal       -> Ok 0
        | Testing_vote   -> Ok 1
        | Testing        -> Ok 2
        | Promotion_vote -> Ok 3
(**# ifdef __PROTO8__
        | Adoption       -> Ok 4
   #**)
      )
    ~decode:(function
        | 0 -> Ok Proposal
        | 1 -> Ok Testing_vote
        | 2 -> Ok Testing
        | 3 -> Ok Promotion_vote
(**# ifdef __PROTO8__
        | 4 -> Ok Adoption
   #**)
        | _ -> assert false
      )
    int

let k =
  custom
    ~encode:(fun a -> Ok (Contract.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Contract.of_b58check a))
    string

let tez =
  custom
    ~encode:(fun a -> Ok (Tez.to_mutez a))
    ~decode:begin fun a ->
      match Tez.of_mutez a with
      | None -> Error "tez"
      | Some t -> Ok t
    end
    int64

let pg_array_of_int_list =
  let rec loop b = function
    | [] -> ()
    | e::(_::_ as tl) ->
      Printf.bprintf b "%d, " e;
      loop b tl
    | [e] ->
      Printf.bprintf b "%d" e
  in
  fun l ->
    let b = Buffer.create 64 in
    Printf.bprintf b "{";
    loop b l;
    Printf.bprintf b "}";
    Buffer.contents b

let int_list_from_string s =
  String.sub s 1 (String.length s - 2)
  |> String.split_on_char ','
  |> List.map (fun e -> String.trim e |> int_of_string)

(* let _ = int_list_from_string "{1,2,3 , 4 }" *)

let slots =
  custom
    ~encode:(fun a -> Ok (pg_array_of_int_list a))
    ~decode:(fun a -> Ok (int_list_from_string a))
    string

let nonce =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct Nonce.encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct Nonce.encoding a))
    json

let lazy_expr_of_json json =
  Data_encoding.Json.destruct Script.lazy_expr_encoding json

let json_of_lazy_expr expr =
  Data_encoding.Json.construct Script.lazy_expr_encoding expr

let lazy_expr =
  custom
    ~encode:(fun a -> Ok (json_of_lazy_expr a))
    ~decode:(fun a -> Ok (lazy_expr_of_json a))
    json

(**# ifdef __PROTO1__
let big_map_diff =
  custom
    ~encode:(function [] -> Ok `Null | _ -> assert false)
    ~decode:(function `Null -> Ok [] | _ -> assert false)
    json
let lazy_storage_diff =
  custom
    ~encode:(function () -> Ok `Null)
    ~decode:(function `Null -> Ok () | _ -> assert false)
    json
#**)(**# elseifdef __PROTO8__
let big_map_diff =
  custom
    ~encode:(function () -> Ok `Null)
    ~decode:(function `Null -> Ok () | _ -> assert false)
    json
let lazy_storage_diff =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct Lazy_storage.encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct Lazy_storage.encoding a))
    json
#**)(**# else
let big_map_diff =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct Contract.big_map_diff_encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct Contract.big_map_diff_encoding a))
    json
let lazy_storage_diff =
  custom
    ~encode:(function () -> Ok `Null)
    ~decode:(function `Null -> Ok () | _ -> assert false)
    json
#**)

let script_of_json json =
  Data_encoding.Json.destruct Script.encoding json

let json_of_script expr =
  Data_encoding.Json.construct Script.encoding expr

let script =
  custom
    ~encode:(fun a -> Ok (json_of_script a))
    ~decode:(fun a -> Ok (script_of_json a))
    json

let balance =
  custom
    ~encode:begin function
      | Delegate.Contract _ -> Ok 0
      | Rewards _ -> Ok 1
      | Fees _ -> Ok 2
      | Deposits _ -> Ok 3
    end
    ~decode:(fun _ -> Error "decode not supported")
    int

let balance_update =
  custom
    ~encode:begin function
      | Delegate.Credited t -> Ok (Tez.to_mutez t)
      | Debited t -> Ok (Int64.neg (Tez.to_mutez t))
    end
    ~decode:begin fun t ->
      let positive = t > 0L in
      match Tez.of_mutez (Int64.abs t) with
      | None -> Error "of_mutez"
      | Some t ->
          if positive then Ok (Delegate.Credited t)
          else Ok (Debited t)
    end
    int64

open Caqti_request

module Block_alpha_table = struct
  let insert =
    create_p
      (tup3
         (tup4 bh pkh int32 cycle)
         (tup4 int32 voting_period int32 voting_period_kind) milligas)
      unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into block_alpha values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        | `Mysql -> "insert ignore into block_alpha values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        | `Pgsql -> "insert into block_alpha values (?, ?, ?, ?, ?, ?, ?, ?, ?::numeric) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end

module Operation_alpha_table = struct
  let insert =
    create_p (tup3 oph int int) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "insert into operation_alpha(hash, id, operation_kind) \
                     values (?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
  let update_sender_receiver =
    let sender = option k
    and receiver = option k in
    create_p (tup4 sender receiver oph int) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "update operation_alpha set \
                     sender = coalesce(?, sender), receiver = coalesce(?, receiver) \
                     where hash = ? and id = ?"
        | _ -> invalid_arg "not implemented"
      end
end

module Manager_numbers = struct
  let insert =
    create_p (tup2 (tup2 oph int) (tup3 z z z)) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into manager_numbers values (?, ?, ?, ?, ?)"
        | `Mysql -> "insert ignore into manager_numbers values (?, ?, ?, ?, ?)"
        | `Pgsql -> "insert into manager_numbers values (?, ?, ?::numeric, ?::numeric, ?::numeric) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

end

module Contract_table = struct
  type t = {
    k: Contract.t ;
    bh : Block_hash.t ;
    manager: Signature.Public_key_hash.t(**# ifndef __META1__  option#**);
    delegate: Signature.Public_key_hash.t option ;
    spendable: bool option;
    delegatable: bool option;
    credit: Tez.t option ;
    preorigination: Contract.t option ;
    script: Script.t option ;
  }

  let sql_encoding =
    custom
      ~encode:begin fun { k ; bh ; manager ; delegate ;
                          spendable ; delegatable ;
                          credit ; preorigination ; script } ->
        Ok ((k, bh, manager),
            (delegate, spendable, delegatable),
            (credit, preorigination, script))
      end
      ~decode:begin
        fun ((k, bh, manager),
             (delegate, spendable, delegatable),
             (credit, preorigination, script)) ->
          Ok { k ; bh ; manager ; delegate ;
               spendable ; delegatable ;
               credit ; preorigination ; script }
      end
      (tup3
         (tup3 k bh (**# ifdef __META1__ pkh #**)(**# else (option pkh)#**))
         (tup3 (option pkh) (option bool) (option bool))
         (tup3 (option tez) (option k) (option script))
         )

  let upsert =
    create_p sql_encoding unit Caqti_mult.zero_or_one
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "select insert_or_update_contract ($1, $2, $3, $4, $5, $6, $7, $8, $9)"
        | _ -> invalid_arg "not implemented"
      end

  let update_script =
    create_p (tup2 k script) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "with c as (select autoid from contract where address = $1 and script is null order by autoid desc limit 1) \
                     update contract c2 set script = $2 from  c where c2.autoid = c.autoid"
        | _ -> invalid_arg "not implemented"
      end

  let get_scriptless_contracts =
    create_p unit k Caqti_mult.zero_or_more
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql ->
          "select distinct(address) \
           from contract c1 \
           where c1.script is null \
           and c1.address like 'K%' \
           and not exists (select * from contract c2 where c2.address = c1.address and c2.script is not null)"
        | _ -> invalid_arg "not implemented"
      end


  let insert_balance =
    create_p (tup3 k bh tez) unit Caqti_mult.zero
      (fun _ ->
         "insert into contract_balance (address, block_hash, balance, block_level) values ($1, $2, $3, (select level from block where hash = $2)) on conflict do nothing"
      )

  let update_delegate =
    create_p (tup3 (option pkh) k bh) unit Caqti_mult.zero
      (fun _di -> "insert into contract (delegate, address, block_hash) values ($1, $2, $3) on conflict (address, block_hash) do update set
 delegate = $1 where contract.address = $2 and contract.block_hash = $3")

  let upsert_discovered =
    create_p (tup2 (tup3 k bh (option pkh)) (tup3 (option bool) (option bool) (option script))) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql ->
          "insert into contract (address, block_hash, mgr, spendable, delegatable, script)  \
           values ($1, $2, $3, $4, $5, $6) on conflict (address, block_hash) do update set \
           mgr = $3, \
           spendable = $4, \
           delegatable = $5, \
           script = $6 \
           where contract.address = $1 and contract.block_hash = $2"
        | _ -> invalid_arg "not implemented"
      end


end

module Balance_table = struct
  type t = (Delegate.balance * Delegate.balance_update) list

  type table = {
    bh: Block_hash.t ;
    op: (Operation_hash.t * int) option ;
    balance: Delegate.balance ;
    k: Contract.t option ;
    cycle: Cycle.t option ;
    diff: Delegate.balance_update ;
  }

  let table =
    custom
      ~encode:begin fun { bh ; op ;
                          balance ; k ; cycle ; diff } ->
        let op_hash, op_id =
          match op with
          | Some (op_hash, op_id) -> Some op_hash, Some op_id
          | None -> None, None in
        Ok ((bh, op_hash, op_id, balance), (k, cycle, diff))
      end
      ~decode:begin fun ((bh, op_hash, op_id, balance),
                         (k, cycle, diff)) ->
        let op =
          match op_hash, op_id with
          | Some op_hash, Some op_id -> Some (op_hash, op_id)
          | _ -> None in
        Ok { bh ; op ; balance ; k ; cycle ; diff }
      end
      (tup2
         (tup4 bh (option oph) (option int) balance)
         (tup3 (option k) (option cycle) balance_update))

  let update_balance =
    create_p table unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "with i as ( \
                     insert into balance values (?, ?, ?, ?, ?, ?, ?) \
                     returning operation_hash, op_id, contract_address, diff \
                     ) update operation_alpha set \
                     receiver = coalesce(contract_address, receiver) \
                     from i \
                     where hash = i.operation_hash and id = i.op_id and i.diff > 0"
        | _ -> invalid_arg "not implemented"
      end

  let update ?op conn ~bh (t: t) =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    with_transaction conn update_balance @@
    List.map begin fun (balance, diff) ->
      match balance with
      | Delegate.Contract k ->
          { k = Some k ; bh ; op ;
            balance ; cycle = None ; diff }
      | Rewards (pkh, cycle)
      | Fees (pkh, cycle)
      | Deposits (pkh, cycle) ->
          let k = Contract.implicit_contract pkh in
          { k = Some k ; bh ; op ;
            balance ; cycle = Some cycle; diff }
    end t
end

module Origination_table = struct
  type t = {
    op: Operation_hash.t ;
    op_id: int ;
    src: Contract.t ;
    k: Contract.t ;
    consumed_gas : Fpgas.t ;
    storage_size : Z.t ;
    paid_storage_size_diff : Z.t ;
    big_map_diff : Contract.big_map_diff option ;
    lazy_storage_diff : Contract.lazy_storage_diff option ;
  }

  let encoding =
    custom
      ~encode:(fun { op ; op_id ; src ; k ; consumed_gas ; storage_size ;
                     paid_storage_size_diff ; big_map_diff ; lazy_storage_diff } ->
                Ok ((op, op_id, src, k),
                    (consumed_gas, storage_size,
                     paid_storage_size_diff, big_map_diff)
                   , lazy_storage_diff))
      ~decode:(fun ((op, op_id, src, k),
                    (consumed_gas, storage_size,
                     paid_storage_size_diff, big_map_diff)
                   , lazy_storage_diff) ->
                Ok { op ; op_id ; src ; k ; consumed_gas ;
                     storage_size ; paid_storage_size_diff ; big_map_diff ; lazy_storage_diff })
      (tup3
         (tup4 oph int k k)
         (tup4 milligas z z (option big_map_diff))
         (option lazy_storage_diff)
      )

  let insert =
    create_p encoding unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql -> "insert into origination values (?, ?, ?, ?, ?::numeric, ?::numeric, ?::numeric, coalesce(?,?)::jsonb) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let select_by_source =
    create_p pkh encoding Caqti_mult.zero_or_more
      (fun _di -> "select * from origination where source = ?")

end

module Tx_table = struct
  type t = {
    op : Operation_hash.t ;
    op_id : int ;
    source : Contract.t ;
    destination : Contract.t ;
    fee : Tez.t ;
    amount : Tez.t ;
    parameters : Script.lazy_expr option ;
    storage : Script.lazy_expr option ;
    consumed_gas : Fpgas.t ;
    storage_size : Z.t ;
    paid_storage_size_diff : Z.t ;
    entrypoint : string option;
    big_map_diff : Contract.big_map_diff option ;
    lazy_storage_diff : Contract.lazy_storage_diff option ;
  }

  let encoding =
    custom
      ~encode:begin fun { op ; op_id ; source ; destination ;
                          fee ; amount ; parameters ; storage ;
                          consumed_gas ; storage_size ; paid_storage_size_diff ;
                          entrypoint ; big_map_diff ; lazy_storage_diff
                        } ->
        Ok ((op, op_id, source, destination)
           , (fee, amount, parameters, storage)
           , (consumed_gas, storage_size, paid_storage_size_diff)
           , (entrypoint, big_map_diff, lazy_storage_diff)
           )
      end
      ~decode:begin fun ((op, op_id, source, destination),
                         (fee, amount, parameters, storage),
                         (consumed_gas, storage_size, paid_storage_size_diff),
                         (entrypoint, big_map_diff, lazy_storage_diff)
                        ) ->
        Ok { op ; op_id ; source ; destination ;
             fee ; amount ; parameters ; storage ;
             consumed_gas ; storage_size ; paid_storage_size_diff ;
             entrypoint ; big_map_diff ; lazy_storage_diff ;
           }
      end
      (tup4
         (tup4 oph int k k)
         (tup4 tez tez (option lazy_expr) (option lazy_expr))
         (tup3 milligas z z)
         (tup3 (option string) (option big_map_diff) (option lazy_storage_diff))
      )

  let insert =
    create_p encoding unit Caqti_mult.zero_or_one
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql ->
          "select insert_or_update_tx ($1, $2, $3, $4, $5, $6, $7, $8,
$9::numeric, $10::numeric, $11::numeric, $12, coalesce($13, $14::jsonb))"


        | _ -> invalid_arg "not implemented"
      end
end

module Delegation_table = struct
  let insert =
    create_p (tup4 oph int k (option pkh)) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into delegation values (?, ?, ?, ?)"
        | `Mysql -> "insert ignore into delegation values (?, ?, ?, ?)"
        | `Pgsql -> "insert into delegation values (?, ?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end


module Implicit_table2 = struct
  let upsert_reveal =
    create_p
      (tup3 k oph pk)
      unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Pgsql ->
          "insert into implicit (pkh, revealed, pk) values \
           ($1, $2, $3) on conflict (pkh) do update set revealed = $2, \
           pk = $3 where implicit.pkh = $1"
        | _ -> invalid_arg "not implemented"
      end
end

module Addresses = struct
  let insert =
    create_p
      k
      unit
      Caqti_mult.zero
      begin fun _ ->
        "insert into addresses values(?) on conflict do nothing"
      end
end



module Endorsement_table = struct
  type t = {
    op_hash : Operation_hash.t;
    op_id : int;
    level : int32;
    delegate : Signature.Public_key_hash.t ;
    slots : int list ;
  }

  let encoding =
    custom
      ~encode:begin fun { op_hash; op_id; level; delegate; slots } ->
        Ok (
          (op_hash, op_id, level),
          (delegate, slots)
        )
      end
      ~decode:begin fun (
        (op_hash, op_id, level),
        (delegate, slots)
      ) ->
        Ok { op_hash; op_id; level; delegate; slots }
      end
      (tup2
         (tup3 oph int int32)
         (tup2 pkh slots)
      )


  let insert =
    create_p
      encoding
      unit
      Caqti_mult.zero
      begin fun _ ->
        "insert into endorsement (operation_hash, op_id, level, delegate, slots) \
         values ($1, $2, $3, $4, $5) on conflict (operation_hash, op_id, level, delegate, slots) \
         do nothing"
      end
end

module Mempool_operations = struct
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  (* let operation_kind_of_int = function
   * | 0 -> "endorsement"
   * | 1 -> "seed_nonce_revelation"
   * | 2 -> "double_endorsement_evidence"
   * | 3 -> "double_baking_evidence"
   * | 4 -> "activate_account"
   * | 5 -> "proposals"
   * | 6 -> "ballot"
   * | 7 -> "reveal"
   * | 8 -> "transaction"
   * | 9 -> "origination"
   * | 10 -> "delegation"
   * | _ -> assert false *)
  (* let int_of_operation_kind = function
   * | "endorsement" -> 0
   * | "seed_nonce_revelation" -> 1
   * | "double_endorsement_evidence" -> 2
   * | "double_baking_evidence" -> 3
   * | "activate_account" -> 4
   * | "proposals" -> 5
   * | "ballot" -> 6
   * | "reveal" -> 7
   * | "transaction" -> 8
   * | "origination" -> 9
   * | "delegation" -> 10
   * | _ -> assert false *)

  type t = {
    branch: Block_hash.t;
    ophash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Signature.public_key_hash option;
    destination: Contract.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level : int32;
  }

  let string_of_status = function
    | Applied -> "applied"
    | Refused -> "refused"
    | Branch_refused -> "branch_refused"
    | Unprocessed -> "unprocessed"
    | Branch_delayed -> "branch_delayed"
  let status_of_string = function
    | "applied" -> Some Applied
    | "refused" -> Some Refused
    | "branch_refused" -> Some Branch_refused
    | "unprocessed" -> Some Unprocessed
    | "branch_delayed" -> Some Branch_delayed
    | _ -> None


  let sql_encoding =
    custom
      ~encode:(fun {
          branch;
          ophash;
          status;
          id;
          operation_kind;
          source;
          destination;
          seen;
          json_op;
          context_block_level;
        } -> Ok (branch,
                 context_block_level,
                 (ophash,
                  string_of_status status,
                  id,
                  operation_kind),
                 (source,
                  destination,
                  seen,
                  Lazy.force json_op)))
      ~decode:(function
          | (branch,
             context_block_level,
             (ophash,
              ("applied"|"refused"|"branch_refused"|"branch_delayed"|"unprocessed"
               as status),
              id,
              operation_kind),
             (source,
              destination,
              seen,
              json_op)) ->
            Ok {
              branch;
              context_block_level;
              ophash;
              status =
                (match status_of_string status with
                 | Some s -> s
                 | _ -> assert false);
              id;
              operation_kind;
              source;
              destination;
              seen;
              json_op = lazy json_op;
            }
          | _ -> Error ("status_of_string"))
      (tup4
         bh
         int32
         (tup4 oph string int int)
         (tup4 (option pkh) (option k) float string))

  let insert =
    create_p sql_encoding unit Caqti_mult.zero_or_one
      (fun _di -> "select insert_into_mempool_operation_alpha(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

end

module Seed_nonce_revelation_table = struct

  type t = {
    bh : Block_hash.t;
    op_hash : Operation_hash.t;
    op_id : int;
    level : int32;
    nonce : Nonce.t
  }

  let encoding =
    custom
      ~encode:(fun { bh; op_hash; op_id; level; nonce } ->
          Ok ((bh, op_hash, op_id, level), nonce))
      ~decode:(fun ((bh, op_hash, op_id, level), nonce) ->
          Ok { bh; op_hash; op_id; level; nonce })
      (tup2 (tup4 bh oph int int32) nonce)

  let insert =
    create_p
      encoding
      unit
      Caqti_mult.zero
      begin fun _ ->
        "insert into seed_nonce_revelation \
           (block_hash, operation_hash, op_id, level, nonce) \
         values ($1, $2, $3, $4, $5) \
         on conflict (block_hash, operation_hash, op_id) \
         do update set block_hash = $1 \
         where seed_nonce_revelation.operation_hash = $2 and seed_nonce_revelation.op_id = $3"
      end

end
