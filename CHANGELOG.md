# Changelog

The recent version tags (`major.minor.patch`) follow these rules:

- `major` will be bumped each time it requires to re-run the indexer
  through the entire chain
- `minor` will be bumped when there's significant changes that don't
  require to bump the `major` number
- `patch` will be bumped for small changes that don't break
  compatibility whatsoever

That being said, the most important thing to determe the need to
update your indexer will probably be the descriptions of the changes
themselves.

## 6.2.0 - 2020-12-08
- (mainnet only) automatically fetch default scripts for scriptless pre-babylon KT contracts
  - this new feature should work by simply stopping any v6 and resuming with this version
- fix/optimization: remove some useless (duplicated) SQL queries

## 6.1.1 - 2020-12-02
- add `--db-schema` and `--rejected-blocks-depth`
  - `--db-schema` prints the default full DB schema and exists
  - `--rejected-blocks-depth` allows to specify how far in the past to look for rejected blocks (default is 100, and 0 means not to look at all)

## 6.1.0 - 2020-11-30
- add support for edonet
- drop support for ebetanet

## 6.0.0 - 2020-11-26
- fix deletion of rejected (aka uncle blocks)
  - this was causing the indexer to poorly crash in case a rejected block would actually be more recently indexed than the non-rejected block of the same level

## 5.1.0 - 2020-11-26 **HOTFIX**
- fix rejected block (aka uncle block) deletion

## 5.0.1 - 2020-11-25
- add `--ebetanet` option

## 5.0.0 - 2020-11-25
- support for proto 8 (ebetanet) (note that edonet does not exist yet, it will be supported at a later time)
  - block_alpha.voting_period is now of type jsonb instead of int
    - for now it's just storing integers in jsonb, but in the future it will likely store more complex data
    - for now you may use a double cast to get value as a number:
      - `select ((voting_period->0)::text)::numeric from block_alpha limit 1;`
      - `select ((voting_period->0)::text)::int from block_alpha limit 1;`

## 4.0.4 - 2020-11-26 **HOTFIX** (should've been 4.1.0)
- fix rejected block (aka uncle block) deletion

## 4.0.3 - 2020-11-23
- fix mezos queries for getting operation data

## 4.0.2 - 2020-11-19
- fix: use this if previous v4.x.y failed to compile due to `version.ml` being wrongly generated

## 4.0.1 - 2020-11-18
- fix: delete rejected blocks sequentially (instead of risking doing it concurrently)

## 4.0.0 - 2020-11-18
- fix: manager numbers (the operation ID column was missing)
- add (or activate) more indexes
- record minimal log into database
- add `--version` to make it easier to know which version is running
- add unique constraint to `autoid` columns
- add SQL functions to delete blocks
- rejected blocks are now automatically deleted right after being detected
- new option `--keep-rejected-blocks` to prevent automatic deletion of rejected blocks: if you use this flag, rejected blocks will be marked, but not deleted
- re-fix `--verbosity` option
- move `contract_legacy` view to heavy database scheme option (because that view is a lot too slow)
- add `src/db-schema/drop_tables.sql` to make emptying DB easier

## 3.1.1 - 2020-11-12
- start indexing (almost) immediately (instead of waiting for the next baked block from the node)
- <s>fix `--verbosity` option</s> (commit is missing)
- fix SQL function contracts3 for mezos

## 3.1.0 - 2020-11-09
- automatically detect protocol for mempool indexing (requires updating the SQL schema; does not change SQL tables)

## 3.0.2 - 2020-11-06
- <s>new `--verbosity` option</s> (broken feature, fixed in 3.1.1)

## 3.0.1 - 2020-11-06
- delete SQL view `tx_full`

## 3.0.0 - 2020-11-05

**This release contains quite a significant amount of changes.
Please wait a few days after its release before using it, to see if bug fixes follow shortly or not.
But feel free to try it immediately and report bugs (if any)!**

### Changes requiring to re-index the whole chain
- keep all balance updates in new `contract_balance` table
  - update of related SQL functions accordingly
- keep history in `contract` table (Warning: major semantics change if you've been using that table)
  - update of related SQL functions accordingly
- change primary key from address to (address, block_hash) to allow history and simpler semantics
- index "seed nonce revelation" operations
- **in the PG SQL database, numbers encoded in an little-endian hexadecimal data are now directly stored as SQL numbers: this improves human readability when browsing the contents of the database! But it also breaks any reader that would still expect to read HEX data!**

### Other changes
- snapshot blocks can now be indexed concurrently to the watch process
- indexing snapshot blocks is now running forever (instead of doing a single cycle)
- move all DB schema files to src/db-schema
- fix typo: `s/db.*scheme/db.*schema/g`
- mark rejected (aka "uncle" or "forked") blocks during watch process, at each level

## 2.3.1 - 2020-11-03
- support automatic protocol transition during `watch` (i.e., no need to stop and restart the indexer anymore)
- support mainnet's transition to protocol 7 during bootstrap

## 2.3.0 - 2020-11-01
- add indexing of mempool

## 2.2.3 - 2020-10-29
- revert v2.2.2 and re-fix it

## 2.2.2 - 2020-10-28
- fix an issue for delphinet (a foreign key that makes the indexer crash on block 4)

## 2.2.1 - 2020-10-27
- fix: mezos SQL function contracts3

## 2.2.0 - 2020-10-27
- fix `contract` table's upsert!

## 2.0.0 - 2020-10-26

### Changes requiring to re-index the whole chain
- store consumed_milligas instead of consumed_gas (because of future transition to PsDELPH1)
- add indexing of (big_map_diff, consumed_gas, storage_size, paid_storage_size_diff) for originations
- store Z numbers as PG numeric instead of little-endian hex in PG char(64)
- add full indexing of endorsements (cf. `endorsment` table)
- **fix**: update block hash for re-assigned operations
  - happens only when "uncle blocks" happen and existing operations are reassigned to a new block

### Other changes
- add src/mezos-db/mempool.sql for indexing mempool operations
  - mempool operations will be indexed by mezos, not by tezos-indexer at first; that might be moved to tezos-indexer in the future
- reverse order of operations returned by some mezos functions: new order order is from newest to oldest
  - some SQL function names are changed consequently
- minor fix: add 2 missing indexes on addresses in table `operation_alpha`
- cosmetic change: some refactoring (code simplification)
- store all json data as Postgres `jsonb` type instead of text or json

## 1.0.2 - 2020-09-18
- lots of refactoring to make the code easier to maintain
- support for transitioning from PsCARTHA to PsDELPH1
- a lot more logs (getting close to 100% coverage)
- better exit codes
- better exit-on-error messages
- add a few FIXME flags for future improvements

## 1.0.1 - 2020-09-16
- add automatic processing speed measurements in debug mode
- example: `# 1000 blocks processed in 23.054222 seconds (2602.560188/min) - 3000 since 58 seconds ago (3104.616143/min on average)`

## 1.0.0 - 2020-09-15
- [major version bump] DB alteration: spendable and delegatable flags in table `contract` are now nullable
- logs:
  - better: more structured, more parsable, more coherent
  - more: the goal is to have logs for every DB insertion or update
  - block hashes: print full hash for block creations, short hash for in-block operations
- makefile:
  - different rules for getting different versions of the SQL DB scheme, customize your DB by giving privilege to performance or to space usage, get more utility queries (from Mezos)
- add optional SQL index on autoid columns (only the one for operation_alpha's autoid column is activated by default)
- upgrade DB scheme (basic) testing using docker


## 0.9.1
- skipped: became 1.0.0

## 0.9 - 2020-09-11
- add option `--debug` (`--verbose` and `--debug` replace `--debug`), verbose doesn't imply debug, debug doesn't imply verbose
- refactoring logs
  - DB updates/inserts are logged with prefix `> `
  - debug logs are prefixed with `# `
- improve Makefile

## 0.8.1 - 2020-09-10
- fix indexing of snapshot blocks for delphinet
- factorize indexing snapshot blocks code using MPP

## 0.8 - 2020-09-10
- use Tezos v7.4
- support for delphinet
- add options `--delphinet`, `--mainnet`, `--carthagenet`

## 0.7
- add table `addresses` to factorize tz1 and KT1 addresses in the DB
- add columns `sender` and `receiver` in table `operation_alpha`
  - these columns allows to get basic information without looking into more specialized tables (e.g., table `tx`) and also give information about operations that don't have specialized tables

## 0.6
- add Makefile rules to generate SQL statements
- import db.sql from mezos (so that synchronising mezos with tezos-indexer becomes easier)

## 0.5
- [requires reindexing] fix table `implicit`

## 0.4
- indexing of `entrypoint`
- fix indexing of `big_map_diff`


## 0.3
- add autoid in operation_alpha

## 0.2
- more indexed information
- add basic DB scheme testing procedure using docker

## 0.1.9
- use tezos 7.3
  - upgrade dune build
  - upgrade .opam files
  - update Dockerfile


*Prior versions require too much archeology to be reported in this changelog.*
