# The Nomadic Labs `tezos-indexer`

## What is an "indexer"

Say you want to see the last 10 transactions for an account.
If you use `tezos-client` to get the information, you'll have to query the blockchain's
blocks one by one, starting from the latest, until you find 10 of them.
If you wanted all transactions for that account, you'd have to browse the whole
blockchain.

The job of an "indexer" is to make such operations easy and fast, and how it does
that is by *indexing* those information, hence its name.

Indexers are the backbones of block explorers, dApps, and wallets.

## How it works

This indexer connects to a running `tezos-node` in `archive mode`, queries blocks,
and puts them into a database.
We're using [PostgreSQL](https://www.postgresql.org/) to handle the database, a
popular engine, so that you can access the data using virtually any programming
language you want.

The source files for the database schema is available in
[src/db-schema](https://gitlab.com/nomadic-labs/tezos-indexer/blob/master/src/db-schema/).


## Tezos network supports

This branch is meant to support the following networks:
* mainnet
* carthagenet
* delphinet
* edonet (≥v6.1.0)
* ebetanet (v6.0.0 only)

All private networks using only the protocols used by the supported
networks mentioned above should be supported as well. However if you have a
private network that makes a protocol transition that doesn't exist in
the supported networks (for instance, it jumps from protocol 2 to
protocol 4, or regresses from 5 to 4), the transition block will not
be supported by this indexer. However, if you ever had a real need for
that, we may arrange a tailor-made solution.



## Getting the data (i.e., running the indexer)

### `tezos-node` running in mode archive

You'll need a `tezos-node` running in mode archive, otherwise you
won't have anything to index! Also, your node should be up to date
with the Tezos network you're using (or at least the latest block
known by your node should use the latest available protocol),
otherwise what you'll obtain is unspecified.

And, unless you give the information on the command line,
you need to create an appropriate `~/.tezos-client/config` file,
which may look like this:

```
{ "base_dir": "/home/username/.tezos-client",
  "node_addr": "tezos-node-address",
  "node_port": 8732,
  "tls": false,
  "web_port": 8080,
  "confirmations": 0 }
```
Note that "web_port" is not used, you may give it any integer value, it won't matter.


Once the indexer has bootstrapped, it should be fine to use a Tezos
node running in full mode to keep up-to-date. (However should you
need to reboot the indexer with an empty databaase, you'll need a node
running in archive mode again.)

**Warning:** It is advised that that the `tezos-node` you'll
connect to is running the same version of the tezos code as the one
your `tezos-indexer` uses. Otherwise there may be compatibility
issues.

### Set up a PostGres database

First, install postgresql and create a database.
Give its ownership to the user running the indexer.

So it should be something like after you have installed PostGres *and*
it's running.
```
createdb name_of_your_database -O username
```

If you decide to set up a password, it's up to you to provide it
via environment variable `PGPASSWORD` or by any other means that work.
*It's possible that some installations of PostGres will require you
to have a non-empty password.*

### Build the indexer

It's become a little complicated to build the indexer, because building Tezos has become a more difficult.
It's highly recommended that you simply use the [Docker images built by the Gitlab CI](https://gitlab.com/nomadic-labs/tezos-indexer/container_registry/842779), or built by yourself using the provided [Dockerfile](https://gitlab.com/nomadic-labs/tezos-indexer/-/blob/master/Dockerfile).

However if you still want to proceed, here's how:

#### First, build tezos 

You're kind of on your own! But you might find https://gitlab.com/tezos/tezos/-/issues/1007 helpful.

#### Second, get a copy of tezos-indexer

```
git clone https://gitlab.com/nomadic-labs/tezos-indexer.git
cd tezos-indexer
```
#### Third, make tezos accessible for the indexer

Now, either you installed tezos, and it's accessible from the indexer, then you may just jump to the next step, or you need to make a local copy, in which case:
```
git clone https://gitlab.com/tezos/tezos.git
```

Then build the rust dependencies locally:
```
(cd tezos && scripts/install_build_deps.rust.sh)
```
(We are assuming you built tezos successfully, so the step above shouldn't be an issue for you.)

#### Fourth, install the extra dependencies for the indexer and build it
```
opam install -y mpp caqti-dynload caqti-lwt caqti-driver-postgresql
make
```

#### Get the database schema

A simple way to get the schema is:
```
make db-schema-all-default | psql
```

If you have never run the indexer, running
```
echo 'select count(*) from block;' | psql
```
should output something like this:
```
 count
--------
 0
(1 row)
```

However, this indexer is used by various other projects and various needs,
therefore there are a few several other ways to get the database schema:
  1. `make db-schema-all-default` outputs the default complete database schema (most common usage)
    - you may replace `default` by `light` for a database with less SQL indexes and possibly fewer automatically-generated columns
    - you may replace `default` by `heavy` for a database with more SQL indexes and possibly more automatically-generated columns
  1. `make db-schema-default` outputs the default database schema just for indexing the blockchain and the mempool (without the mezos part).
  1. `make db-schema-mempool-solo` outputs the default database schema just for indexing the mempool (without anything else).
  1. `make db-schema-mezos` outputs the extra database schema used by mezos (without the indexing part).

All database schema source code is in `src/db-schema/`.


### Run the indexer for the blockchain

#### For mainnet (the default network):

Run the indexer:
```
./tezos-indexer --verbose --db=postgresql://my-pg-server.example.com/name_of_your_database
```

Note that although by default the indexer will try to connect to and use `postgresql://localhost/chain`,
you should not rely on these default values.

You should drop the `--verbose` option in order to gain speed or if you don't need the logs.


Regarding mainnet, when protocol 5 (PsBabyM1) became active, all contracts were assigned a script (before protocol 5, it was optional).
If you need those scripts to be present in the database, you may want to run a shell script similar to this:
```bash
# get the scripts from the node
for i in $(psql <<< "select distinct(address) from contract where address like 'K%' and script is null;"|grep K) ; do
  echo " -- $i" ; x=$(curl http://localhost:8732/chains/main/blocks/head/context/contracts/$i/script --silent) ;
  echo "update contract set script = '$x' where address = '$i' and script is null ;" ;
done > update-missing-scripts.sql

# check your newly generated update-missing-scripts.sql file
$EDITOR update-missing-scripts.sql

# update your database
psql name_of_your_database < update-missing-scripts.sql
```

#### For edonet

```
./tezos-indexer --edonet
```

#### For delphinet

```
./tezos-indexer --delphinet
```


#### For carthagenet

```
./tezos-indexer --carthagenet
```


To check where the indexer is at, you could do something like this:
```
$ psql name_of_your_database <<< 'select count(*) from block;'
 count
--------
 474987
(1 row)
```
If ever this gives you an error, it means you have issues with your PostGres
installation.

### Run the indexer for the mempool (new and young feature)

We have a new feature that allows to index the contents of the mempool.
When activated, this feature deactivates the indexing of the blockchain.
To index both the blockchain and the mempool, you need to run two instances
of `tezos-indexer`. Preferably, they should share the same database, so that
you may write more powerful requests to retrieve data. However, you may
run just one of them: you do not need to index the blockchain to index
the mempool, nor do you need to index the mempool to index the blockchain.

The mempool indexer is quite simple. The database schema is mainly in
`src/db-schema/mempool.sql` but to get the full database schema required
for indexing the mempool, you should run `make db-schema-mempool-solo`.

#### The mempool indexer works for protocols 6 (PsCARTHA) and 7 (PsDELPH1)

```
./tezos-indexer --mempool-only
```

*Since v3.1.0, the protocol is automatically detected. The need to specify the network on the command line is no longer needed.*

Note that it is expected that your node is synchronized with the blockchain.


### Issues

Most common issues:

* It's been observed that the indexer may get stuck during bootstrap when
the connection to the Tezos node is not good enough. In such a case,
an easy workaround is to just restart it
(i.e., kill it with Ctrl-C and restart it). This issue hasn't been observed
(yet?) when the network connection between the indexer and the node is
flawless.

* If you forgot to initialize the database (by feeding it the schema),
  the indexer will get stuck pretty rapidly and might not tell you
  anything about it.

* If your build fails and `dune` tells you there's a cycling
  dependency issue, you should delete everything that was generated by
  `dune` and start again. You're welcome to open an issue if you run into trouble.

More known issues are being tracked on the Gitlab tracker and getting fixed.


### Precisions

* The "bootstrap" consists in processing the blocks that are known to
already exist: when you start the indexer, it retrieves the current
level (L) from the tezos node, and it'll process all the blocks it
doesn't have between block 2 and L. During that time, new blocks will
come to existence, however they will not be part of the bootstrap,
they'll be treated by the "watcher", which monitors the node for new
blocks, and processes those blocks.

* When you launch the indexer, the first thing it will do is to build
the list of snapshot blocks (a table that associates cycles with the
selected snapshot blocks).
Then it will bootstrap.
Then it will watch.

* "Watching" means waiting on the Tezos node for new blocks.

Hardware requirements
---------------------

### Storage

Size of postgres database <s>(3 June 2019) 24G for mainnet</s> in November 2020, it'll soon take 200 GB for mainnet.
This is because the indexer is indexing more kinds of data than before, and obviously also because the chains are growing.


### RAM

It might work with less than 200MB. If ever it needs more, it'll be
for compiling tezos source code (including opam packages).

It's possible that `dune build` fails if goes short on resources.
In such case, invoke `dune build` again and hopefully it'll move
forward and eventually finish its job.

### CPU

Any reasonable CPU should be fine.

With a 3GHz Core i5 of 2018 on a 100Mbit/s internet access, it could
treat about 150 blocks per minute during bootstrap.

With an old Xeon with similar performance but on a much faster network
(OVH internal network, since both the indexer and the node were in the
same OVH datacenter), it went at about 500 to 3000 blocks per minute.

So it might take a very long time if you use a slow node or have a
slow internet connection. If you index only 1 block per minute, it's
more than enough for live blocks, but it would take at least months
if not years to bootstrap.

In such a case, it would be strongly recommended to download a prefilled
database!

### Network

Ping! If your node and your indexer are not on the same machine or local network
(meaning the ping between the machine running the indexer and the machine running
the node is not negligible), the ping will likely be the bottleneck.
The reason for that is that, at least at the time of writing, there are a number
of HTTP requests happening sequentially between the indexer and the node, and even
if the node took 0 ms to answer, and the indexer took 0 ms to treat the information,
the sequentiality of the requests, with a great ping value, would dramatically limit
the speed of indexing.

If the services run on the same machine or in the same local network, the ping is
usually around 1 ms.
If the machine running the node is physically far from the machine running the indexer,
you'll be limited by twice that distance divided by the speed of light for each
HTTP request even if you have a super high connection speed.

While it's possible to use public nodes to run the indexer, if they're physically
far from you, it'll be very slow.

But then, once you have finished bootstrapping, you'll have only one or two new
blocks to indexer per minute.


##  Work in progress / Future work

cf. https://gitlab.com/nomadic-labs/tezos-indexer/-/issues


Misc commands that might be helpful
-----------------------------------

Peek at the latest blocks
```bash
$ psql name_of_your_database <<< 'select * from block order by level desc limit 10;'
```

Reset the chain database
```bash
# as root
$ su - postgres -c 'dropdb name_of_your_database && createdb name_of_your_database -O username'
```


#### Mezos

Mezos provides an RPC-based API on top of tezos-indexer.

## Development versions

You are welcome to try a branch that has the `-dev` suffix: it indicates that
it might be in a testing phase.

## Contributions

Bug reports are welcome and merge requests as well.

## Issues or questions

You're very welcome to [raise issues if you have any](https://gitlab.com/nomadic-labs/tezos-indexer/issues) and/or to [submit merge requests](https://gitlab.com/nomadic-labs/tezos-indexer/merge_requests).

## Contact

You're welcome to create issues or merge requests.
You may also contact [Philippe Wang](@philippewang.info) by other means (email, slack,
messenger, sms, telegram, linkedin, etc.) as long as it works.
You may also contact any member of Nomadic Labs.
And if you want your name added here, please submit a merge request! ;)
