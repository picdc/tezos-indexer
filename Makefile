# Open Source License
# Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


SHELL=/bin/bash

all:
	@$(MAKE) tezos
	@$(MAKE) protos
	@$(MAKE) build

build:
	dune build ./_build/default/src/bin_indexer/main_indexer.exe
	cp -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer

tezos:
	@echo "**************************************************************************"
	@echo "You're expected to have tezos installed and accessible, or have a copy here,"
	@echo "or this will fail."
	@echo "If you don't have it installed and accessible, you may run this to make a copy here:"
	@echo "git clone https://gitlab.com/tezos/tezos.git"
	@echo "And then build the rust dependencies:"
	@echo "(cd tezos && scripts/install_build_deps.rust.sh)"
	@echo "**************************************************************************"


GENERATED_FILES = \
	src/bin_indexer/db_schema.ml \
	src/bin_indexer/version.ml \
	src/bin_indexer/snapshot_utils_6.ml \
	src/bin_indexer/snapshot_utils_7.ml \
	src/bin_indexer/snapshot_utils_6.mli \
	src/bin_indexer/snapshot_utils_7.mli \
	src/proto_001_PtCJ7pwo \
	src/proto_002_PsYLVpVv \
	src/proto_003_PsddFKi3 \
	src/proto_004_Pt24m4xi \
	src/proto_005_PsBabyM1 \
	src/proto_006_PsCARTHA \
	src/proto_007_PsDELPH1 \
	src/proto_008_PtEdoTez \
	src/bin_indexer/chain_db_transition_001_002.ml  \
	src/bin_indexer/chain_db_transition_001_002.mli \
	src/bin_indexer/chain_db_transition_002_003.ml  \
	src/bin_indexer/chain_db_transition_002_003.mli \
	src/bin_indexer/chain_db_transition_003_004.ml  \
	src/bin_indexer/chain_db_transition_003_004.mli \
	src/bin_indexer/chain_db_transition_004_005.ml  \
	src/bin_indexer/chain_db_transition_004_005.mli \
	src/bin_indexer/chain_db_transition_005_006.ml  \
	src/bin_indexer/chain_db_transition_005_006.mli \
	 src/bin_indexer/mempool_utils_transition_006_007.ml  \
	 src/bin_indexer/chain_db_transition_006_007.ml  \
	 src/bin_indexer/chain_db_transition_006_007.mli

protos: $(GENERATED_FILES)

src/bin_indexer/version.ml:Makefile
	printf 'let c = "(commit: %s %s)"' "$$(git show|head -n 1|sed 's/commit //')" "$$(git log|head -n 3|tail -n 1)" > $@
	printf 'let t = "%s"' $$(git log --decorate=full |head -n 1|grep 'tag: refs/tags/'| sed 's|.*tag: refs/tags/||'|tr -d ')'|sed 's/,.*//') >> $@
	printf 'let t = if t <> "" then t else "(dev)"' >> $@
	printf 'let b = "(branch: %s)"' "$$(git branch |grep '*'|tr -d '*()')" >> $@
	echo 'let version () = Printf.printf "tezos-indexer %s %s %s\n%!" t b c; exit 0' >> $@


src/bin_indexer/db_schema.ml:Makefile src/db-schema/*
	>&2 ${MAKE} -s --no-print-directory db-schema-all-default > tmp.sql
	echo 'really_input_string stdin (in_channel_length stdin) |> Printf.printf "let s = %S let print () = print_endline s"' > tmp.ml
	ocaml tmp.ml < tmp.sql > $@
	rm -f tmp.sql tmp.ml

MPP = mpp -so '' -sc '' -t ocaml -son "(**\#" -scn "\#**)" -sos-noloc '' -sos '' -scs '' -soc '' -scc '' -sec ''
BIGMAPDIFF =  -set __BIG_MAP_DIFF__
STORAGEDIFF =  -set __STORAGE_DIFF__
MILLIGAS = -set __MILLIGAS__
META1 =  -set __META1__
META2 =  -set __META2__
META3 =  -set __META3__

MEMPOOL = -set __MEMPOOL__

PROTO1 = ${MPP}               ${META1} -set PROTO=001_PtCJ7pwo -set PROTONEXT=001_PtCJ7pwo -set PROTONAME=001-PtCJ7pwo -set __PROTO1__
PROTO2 = ${MPP} ${BIGMAPDIFF} ${META1} -set PROTO=002_PsYLVpVv -set PROTONEXT=002_PsYLVpVv -set PROTONAME=002-PsYLVpVv -set __PROTO2__
PROTO3 = ${MPP} ${BIGMAPDIFF} ${META1} -set PROTO=003_PsddFKi3 -set PROTONEXT=003_PsddFKi3 -set PROTONAME=003-PsddFKi3 -set __PROTO3__
PROTO4 = ${MPP} ${BIGMAPDIFF} ${META1} -set PROTO=004_Pt24m4xi -set PROTONEXT=004_Pt24m4xi -set PROTONAME=004-Pt24m4xi -set __PROTO4__
PROTO5 = ${MPP} ${BIGMAPDIFF} ${META2} -set PROTO=005_PsBabyM1 -set PROTONEXT=005_PsBabyM1 -set PROTONAME=005-PsBabyM1 -set __PROTO5__
PROTO6 = ${MPP} ${BIGMAPDIFF} ${META2} -set PROTO=006_PsCARTHA -set PROTONEXT=006_PsCARTHA -set PROTONAME=006-PsCARTHA -set __PROTO6__ ${MEMPOOL}
PROTO7 = ${MPP} ${BIGMAPDIFF}  ${META2} -set PROTO=007_PsDELPH1 -set PROTONEXT=007_PsDELPH1 -set PROTONAME=007-PsDELPH1 -set __PROTO7__ ${MEMPOOL} ${MILLIGAS}
PROTO8 = ${MPP} ${STORAGEDIFF} ${META3} -set PROTO=008_PtEdoTez -set PROTONEXT=008_PtEdoTez -set PROTONAME=008-PtEdoTez -set __PROTO8__ ${MEMPOOL} ${MILLIGAS}
TRANS = -set __TRANSITION__
TRANS_1_2 = ${PROTO1} ${TRANS} -set __TRANSITION_1_2__ -set PROTONEXT=002_PsYLVpVv
TRANS_2_3 = ${PROTO2} ${TRANS} -set __TRANSITION_2_3__ -set PROTONEXT=003_PsddFKi3
TRANS_3_4 = ${PROTO3} ${TRANS} -set __TRANSITION_3_4__ -set PROTONEXT=004_Pt24m4xi
TRANS_4_5 = ${PROTO4} ${TRANS} -set __TRANSITION_4_5__ -set PROTONEXT=005_PsBabyM1
TRANS_5_6 = ${PROTO5} ${TRANS} -set __TRANSITION_5_6__ -set PROTONEXT=006_PsCARTHA
TRANS_6_7 = ${PROTO6} ${TRANS} -set __TRANSITION_6_7__ -set PROTONEXT=007_PsDELPH1


src/bin_indexer/snapshot_utils_6.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO6} $< > $@
src/bin_indexer/snapshot_utils_6.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO6} $< > $@

src/bin_indexer/snapshot_utils_7.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO7} $< > $@
src/bin_indexer/snapshot_utils_7.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO7} $< > $@
src/bin_indexer/snapshot_utils_8.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO8} $< > $@


src/proto_001_PtCJ7pwo:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO1} > $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO1} > $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO1} > $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO1} > $@/db_alpha.mli
	${PROTO1} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-001-PtCJ7pwo.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_002_PsYLVpVv:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO2} >  $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO2} >  $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO2} >  $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO2} >  $@/db_alpha.mli
	${PROTO2} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-002-PsYLVpVv.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_003_PsddFKi3:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO3} >  $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO3} >  $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO3} >  $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO3} >  $@/db_alpha.mli
	${PROTO3} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-003-PsddFKi3.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_004_Pt24m4xi:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO4} >  $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO4} >  $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO4} >  $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO4} >  $@/db_alpha.mli
	${PROTO4} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-004-Pt24m4xi.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_005_PsBabyM1:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO5} > $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO5} > $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO5} > $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO5} > $@/db_alpha.mli
	${PROTO5} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-005-PsBabyM1.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_006_PsCARTHA:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO6} > $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO6} > $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO6} > $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO6} > $@/db_alpha.mli
	cat src/proto_meta/mempool_utils.ml | ${PROTO6} > $@/mempool_utils.ml
	${PROTO6} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-006-PsCARTHA.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_007_PsDELPH1:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO7} > $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO7} > $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO7} > $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO7} > $@/db_alpha.mli
	cat src/proto_meta/mempool_utils.ml | ${PROTO7} > $@/mempool_utils.ml
	${PROTO7} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-007-PsDELPH1.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_008_PtEdoTez:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	cat src/proto_meta/chain_db.ml  | ${PROTO8} > $@/chain_db.ml
	cat src/proto_meta/chain_db.mli | ${PROTO8} > $@/chain_db.mli
	cat src/proto_meta/db_alpha.ml  | ${PROTO8} > $@/db_alpha.ml
	cat src/proto_meta/db_alpha.mli | ${PROTO8} > $@/db_alpha.mli
	cat src/proto_meta/mempool_utils.ml | ${PROTO8} > $@/mempool_utils.ml
	${PROTO8} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-008-PtEdoTez.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/bin_indexer/chain_db_transition_001_002.ml:src/proto_meta/chain_db.ml Makefile
	cat $< | ${TRANS_1_2} > $@

src/bin_indexer/chain_db_transition_001_002.mli:src/proto_meta/chain_db.mli Makefile
	cat $< | ${TRANS_1_2} > $@

src/bin_indexer/chain_db_transition_002_003.ml:src/proto_meta/chain_db.ml Makefile
	cat $< | ${TRANS_2_3} >  $@

src/bin_indexer/chain_db_transition_002_003.mli:src/proto_meta/chain_db.mli Makefile
	cat $< | ${TRANS_2_3} >  $@

src/bin_indexer/chain_db_transition_003_004.ml:src/proto_meta/chain_db.ml Makefile
	cat $< | ${TRANS_3_4} >  $@

src/bin_indexer/chain_db_transition_003_004.mli:src/proto_meta/chain_db.mli Makefile
	cat $< | ${TRANS_3_4} >  $@

src/bin_indexer/chain_db_transition_004_005.ml:src/proto_meta/chain_db.ml Makefile
	cat $< | ${TRANS_4_5} >  $@

src/bin_indexer/chain_db_transition_004_005.mli:src/proto_meta/chain_db.mli Makefile
	cat $< | ${TRANS_4_5} >  $@

src/bin_indexer/chain_db_transition_005_006.ml:src/proto_meta/chain_db.ml Makefile
	cat $< | ${TRANS_5_6} > $@

src/bin_indexer/chain_db_transition_005_006.mli:src/proto_meta/chain_db.mli Makefile
	cat $< | ${TRANS_5_6} > $@

src/bin_indexer/chain_db_transition_006_007.ml:src/proto_meta/chain_db.ml Makefile
	cat $< | ${TRANS_6_7} > $@

src/bin_indexer/chain_db_transition_006_007.mli:src/proto_meta/chain_db.mli Makefile
	cat $< | ${TRANS_6_7} > $@

src/bin_indexer/mempool_utils_transition_006_007.ml:src/proto_meta/mempool_utils.ml Makefile
	cat $< | ${TRANS_6_7} > $@


clean:
	find . -name '*~g' -delete
	rm -fr tezos-indexer $(GENERATED_FILES)
	rm -fr _build src/bin_indexer/.merlin src/bin_indexer/tezos-indexer.install src/lib_indexer/.merlin src/lib_indexer/tezos-indexer-lib.install

test-psql:
	bash test-psql.bash

db-schema-help:
	@echo "/****************************************************************"
	@echo "If you need just the indexer, run:"
	@echo "make db-schema-default # or"
	@echo "make db-schema-light # if you want a lighter DB (less space required), or"
	@echo "make db-schema-heavy # if you want faster DB reads (more SQL indexes, therefore more space required)"
	@echo "If you also want additional SQL accessors (functions, views, etc.), run:"
	@echo "make db-schema-all-default # or"
	@echo "make db-schema-all-light # if you want a lighter DB (less space required), or"
	@echo "make db-schema-all-heavy # if you want faster DB reads (more SQL indexes, therefore more space required)"
	@echo "****************************************************************/"


db-schema:
	@${MAKE} -s --no-print-directory db-schema-help
	@echo "-- If you want to immediately run \`make db-schema-default\`, press ENTER, else Ctrl-C" ; read
	@${MAKE} -s --no-print-directory db-schema-default

db-schema-light:
	@echo "-- BETA: trying to generate a much lighter database, which might come at the cost of some convenience and performance"
	@${MAKE} -s --no-print-directory db-schema-medium | sed -e 's/.*--OPT.*//'

db-schema-heavy:
	@${MAKE} -s --no-print-directory db-schema-medium | sed -e 's/--OPT//'

db-schema-default:db-schema-medium
db-schema-medium:
	@${MAKE} -s --no-print-directory db-schema-chain
	@${MAKE} -s --no-print-directory db-schema-mempool

db-schema-chain:
	@echo "-- src/db-schema/addresses.sql"
	@cat src/db-schema/addresses.sql
	@echo "-- src/db-schema/chain.sql"
	@cat src/db-schema/chain.sql

db-schema-mempool:
	@echo "-- src/db-schema/mempool.sql"
	@cat src/db-schema/mempool.sql

db-schema-mempool-solo:
	@echo "-- src/db-schema/addresses.sql"
	@cat src/db-schema/addresses.sql
	@echo "-- src/db-schema/mempool.sql"
	@cat src/db-schema/mempool.sql


db-schema-all-default:db-schema-all-medium
db-schema-all-medium:
	@${MAKE} -s --no-print-directory db-schema-medium
	@${MAKE} -s --no-print-directory db-schema-mezos
db-schema-all-heavy:
	@${MAKE} -s --no-print-directory db-schema-heavy
	@${MAKE} -s --no-print-directory db-schema-mezos | sed -e 's/--OPT //'
db-schema-all-light:
	@echo "-- BETA: trying to generate a much lighter database, which might come at the cost of some convenience and performance"
	@${MAKE} -s --no-print-directory db-schema-light
	@${MAKE} -s --no-print-directory db-schema-mezos | sed -e 's/.*--OPT.*//'
db-schema-mezos:
	@${MAKE} -s --no-print-directory db-schema-mezos-base
	@${MAKE} -s --no-print-directory db-schema-mezos-extra
db-schema-mezos-base:
	@echo "-- src/mezos-db/db.sql"
	@cat src/db-schema/mezos.sql
db-schema-mezos-extra:
	@echo "-- ocaml src/db-schema/mezos_gen_sql_queries.ml"
	@ocaml src/db-schema/mezos_gen_sql_queries.ml

.PHONY: clean protos test-psql db-schema-all-heavy db-schema-all-light db-schema-mezos db-schema-mezos-base db-schema-mezos-extra db-schema-all-default db-schema-all-medium db-schema

docker-build:
	rm -fr docker && mkdir docker
	cp -a .git docker && cd docker && git checkout .
	cp -af Dockerfile src Makefile docker/
	cd docker && git commit -a -m "dev $$(date)" ; docker build .
