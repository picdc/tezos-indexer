# Open Source License
# Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

FROM registry.gitlab.com/tezos/opam-repository:605cefe2c938b3995ef1a82ff2701bafd5d634cb as build

USER root
RUN apk add opam alpine-sdk m4 gmp-dev postgresql-dev perl libev-dev libffi libffi-dev
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/community hidapi-dev

USER tezos
WORKDIR /home/tezos
ENV OPAMYES=true

RUN opam remote add default https://opam.ocaml.org

RUN opam install caqti-dynload caqti-lwt caqti-driver-postgresql mpp

RUN git clone https://gitlab.com/tezos/tezos.git -b master --single-branch

# the following line is meant to cache part of the compilation, it may be safely deactivated
RUN eval $(opam env) && dune build _build/default/tezos/src/bin_client/main_client.exe

COPY Makefile .
COPY src src
COPY .git .git
RUN sudo chown -R tezos src
RUN eval $(opam env) && make protos
RUN eval $(opam env) && dune build ./_build/default/src/bin_indexer/main_indexer.exe
RUN cp -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer
RUN rm -rf _build tezos
RUN /home/tezos/tezos-indexer --version
ENTRYPOINT ["/home/tezos/tezos-indexer"]
